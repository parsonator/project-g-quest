using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseIcon : MonoBehaviour {

    static CurruntAction curruntAction;

    [SerializeField] Image Icon;

    [SerializeField] List<Sprite> Icons;
    void Update() {
        Vector2 mousePosition = Input.mousePosition;
        Icon.sprite = Icons[(int) curruntAction];
        Icon.rectTransform.position = mousePosition;
    }

    public static void SetSpriteState(CurruntAction _curruntAction) {
        curruntAction = _curruntAction;
    }
}