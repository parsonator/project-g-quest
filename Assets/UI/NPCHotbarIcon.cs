﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// [ExecuteInEditMode]
public class NPCHotbarIcon : MonoBehaviour {

	public LivingEntity entity;

	public Text entityText,entityHPText;
	public Image entityImage;



	// Use this for initialization
	void Start () {
		Setup();
	}

	public void Setup()
	{
		entityText.text = entity.data.firstName;
		entityHPText.text = entity.totalHealth.ToString();
		entityHPText.color = entity.GetHealthColor();
		entityImage.sprite = entity.data.icon;
		
	}
	
	// Update is called once per frame
	void Update () {
		entityHPText.text = entity.totalHealth.ToString();
		entityHPText.color = entity.GetHealthColor();
	}
}
