﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectiveMessagePanel : MonoBehaviour {

	public Text titleText;
	public Text missionText;

	public void CurruntMissionText(GObjective _objective) {
		titleText.text = _objective.curruntObjective.objectiveName;
		missionText.text = _objective.GetQuestInfo();
	}

}