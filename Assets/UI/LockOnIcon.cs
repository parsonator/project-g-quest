﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LockOnIcon : Icon {

    [SerializeField]
    Image pointIcon;
    PlayerEntity playerEntity;
    Entity entity;
    Camera cam;

    public void SetIcon(Entity entity) {
        display = false;
        playerEntity = PlayerEntity.main;

        cam = Camera.main;
        target = playerEntity.transform;
    }

    void Update() {
        if (playerEntity.eTargeting && playerEntity.eTarget != null) {
            display = true;
            entity = playerEntity.eTarget;
            target = entity.transform;
        } else {
            display = false;
        }

    }
}