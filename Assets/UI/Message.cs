﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Message{
	[Tooltip("{0} = Target fullname| {1} = Target fist name| {2} Target race")]
	[TextArea(2,2)]
	public string MessageString;
	public string outputString{
		get{
			if(target != null){
			return string.Format(MessageString,
				target.data.fullName,
				target.data.firstName);
			}
			return MessageString;
		}
	}
	public LivingEntity target;

	[HideInInspector]
	public List<MessageOptions> options = new List<MessageOptions>();

	public Message (string _message)
	{
		MessageString = _message;
	}
	public Message (string _message,LivingEntity _target)
	{
		MessageString = _message;
		target = (LivingEntity)_target;
	}

	public void SetLastMessage()
	{
            MessageOptions nextButton = new MessageOptions(
                delegate ()
                {
                  DialogueSystem.StopTalking();
                }
              , "End"
            );
            options.Add(nextButton);
	}
}
