using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// [CreateAssetMenu(fileName = "GMessageOptions", menuName = "GQuest/Dialogue/Message", order = 1)]
[System.Serializable]
public class MessageOptions {
    [TextArea(2, 2)]
    public string optionMessage;

    public delegate void onOptionSelect();
    public onOptionSelect optionSelected;
    public Queue<Message> MessageQueue;

    public MessageOptions(onOptionSelect _onOptionSelect, string _optionMessage) {
        optionSelected += _onOptionSelect;
        optionMessage = _optionMessage;
    }

}