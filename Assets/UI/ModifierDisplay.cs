﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifierDisplay : MonoBehaviour {

	public GameObject icon;

	// Use this for initialization
	void Start() {
		PlayerEntity.main.data.OnTraitChange += RedrawMods;
		RedrawMods();
	}

	void RedrawMods() {
		for (int i = 0; i < transform.childCount; i++)
			Destroy(transform.GetChild(i).gameObject);

		foreach (var t in PlayerEntity.main.ActiveListOfTraits) {
			foreach (var mods in t.Modifiers) {
				if ((mods as BaceRaceModifier))
					continue;

				GameObject modIcon = Instantiate(icon, transform);
				modIcon.GetComponent<TraitsHotbarIcon>().modifier = mods;
			}
		}

	}

	// // Update is called once per frame
	// float t;
	// void Update () {
	// 	t += Time.deltaTime;
	// 	if(t>10){
	// 		t=0;
	// 		RedrawMods();
	// 	}
	// }
}