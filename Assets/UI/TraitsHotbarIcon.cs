﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// [ExecuteInEditMode]
public class TraitsHotbarIcon : MonoBehaviour {

	public Modifier modifier;
	public Text entityText;
	public Image entityImage;



	// Use this for initialization
	void Start () {
		ReDraw();
	}

	public void ReDraw()
	{
		entityText.text = modifier.name;
		entityImage.sprite = modifier.icon;
	}


}
