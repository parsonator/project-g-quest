﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Icon : MonoBehaviour {

    Camera cam;

    [HideInInspector] public bool activeTrack = true;
    [HideInInspector] public bool display = true;
    public Transform target;
    public Vector3 offset = new Vector2(0, 1.5f);

    bool displayChange;

    protected void Start() {
        cam = Camera.main;
    }
    public void UpdatePoz() {
        Vector3 space = cam.WorldToScreenPoint(target.position + offset);
        Vector3 rez = new Vector3(Screen.width, Screen.height);
        transform.position = space;
        displayChange = !display;
    }

    public bool offscreen() {
        float dot = Vector3.Dot(cam.transform.forward, (target.transform.position - cam.transform.position).normalized);
        return (dot > 0.7f);
    }

    void LateUpdate() {
        Vector3 forward = cam.transform.TransformDirection(Vector3.forward);
        Vector3 toOther = target.position - cam.transform.position;
        activeTrack = (Vector3.Dot(forward, toOther)> 0);
        if (activeTrack)
            UpdatePoz();
        if (displayChange != display) {
            foreach (var item in GetComponentsInChildren<Image>(true)) {
                item.enabled = display;
            }
            foreach (var item in GetComponentsInChildren<Text>(true)) {
                item.enabled = display;
            }
            displayChange = display;
        }

    }
}