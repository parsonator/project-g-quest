﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class DialogueSystem : MonoBehaviour {

    public static DialogueSystem main;

    [Header("Message")]
    public Queue<Message> MessageQueue = new Queue<Message>();
    public Message currentMessage;
    [Header("prefabs")]
    [SerializeField]
    GameObject messageOptionButton;
    [Header("GameObjects")]
    [SerializeField]
    Text dialogueText;
    [SerializeField] Transform dialogueOptionsBox;
    List<DialougeButton> messageOptionButtons = new List<DialougeButton>();
    public static Dictionary<Entity, Queue<Message>> MissionMessages = new Dictionary<Entity, Queue<Message>>();
    public LivingEntity curruntTarget;
    public delegate void EndEvent();
    public static EndEvent onDialogueCompletion;
    public bool windowActive;
    void Awake() {
        main = this;
        SetWindow(false);
    }

    public void SetWindow(bool set) {
        windowActive = false;
        PlayerInput.blockGameActionControles = set;
        for (int i = 0; i < transform.childCount; i++) {
            transform.GetChild(i).gameObject.SetActive(set);
        }
    }

    public static void DisplayMessageQueue(Queue<Message> NewMessageQueue, LivingEntity _entity) {
        main.curruntTarget = _entity;

        if (NewMessageQueue.Count != 0) {
            if (main.MessageQueue.Any()) {
                foreach (var e in NewMessageQueue)
                    main.MessageQueue.Enqueue(e);
            } else main.MessageQueue = NewMessageQueue;

            main.DisplayNextMessage();
            main.SetWindow(true);
            PlayerEntity.main.motion.blockMotion = true;
        } else main.SetWindow(false);
    }

    public static void DisplayOneMessage(string _message, LivingEntity _entity) {
        main.curruntTarget = _entity;
        main.MessageQueue.Enqueue(new Message(_message));
        main.DisplayNextMessage();
        main.SetWindow(true);
        PlayerMovement.main.motion.blockMotion = true;
    }

    public void DisplayNextMessage() {
        CleanWindow();

        if (MessageQueue.Count != 0) {
            currentMessage = MessageQueue.Dequeue();
            dialogueText.text = currentMessage.outputString.Trim();
            DisplayMessageOptions();

        } else {
            currentMessage = null;
            SetWindow(false);
        }
    }

    void DisplayMessage() {
        dialogueText.text = currentMessage.outputString;
    }

    void CleanWindow() {
        dialogueText.text = "";
        foreach (DialougeButton button in messageOptionButtons) {
            Destroy(button.gameObject);
        }
        messageOptionButtons = new List<DialougeButton>();

    }

    void DisplayMessageOptions() {

        if (MessageQueue.Count != 0) {
            MessageOptions nextButton = new MessageOptions(
                delegate() { DisplayNextMessage(); }, "Next"
            );

            currentMessage.options.Add(nextButton);
        } else if (MessageQueue.Count == 0 && currentMessage.options.Count == 0) {
            currentMessage.SetLastMessage();
        }

        if (currentMessage.options.Count > 0) {
            foreach (MessageOptions messageOptions in currentMessage.options) {
                DialougeButton buttonObj = Instantiate(messageOptionButton, dialogueOptionsBox).GetComponent<DialougeButton>();
                buttonObj.SetUpButton(messageOptions);
                messageOptionButtons.Add(buttonObj);
            }
        }

    }

    public static void StopTalking() {
        if (onDialogueCompletion != null)
            onDialogueCompletion();

        PlayerEntity.main.motion.blockMotion = false;
        main.SetWindow(false);
    }

    private void Update()
    {
        if(windowActive)
        {
            if(PlayerInput.InteractDown())
                SetWindow(false);
        }
    }

}