﻿using System.Collections;
using System.Collections.Generic;
using GQuest;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	public ObjectiveMessagePanel objectiveMessagePanel;

	public static UIManager main;

	List<ObjectiveMessagePanel> allPanels = new List<ObjectiveMessagePanel>();

	public void curruntMissionText(GNode node) {

		foreach (var panel in allPanels)
			Destroy(panel.gameObject);
		allPanels = new List<ObjectiveMessagePanel>();

		foreach (var op in node.outEdges) {
			if(op.objective == null) continue;
			ObjectiveMessagePanel p = Instantiate(objectiveMessagePanel,transform).GetComponent<ObjectiveMessagePanel>();
			p.CurruntMissionText(op.objective);
			allPanels.Add(p);
		}
	}

	// public void updateMissionText(string text) {
	// 	foreach (var panel in allPanels)

	// 	missionText.text = text;
	// 	missionName.text = "";
	// }

	// Use this for initialization
	void Awake() {
		main = this;
	}

	// Update is called once per frame
	void Update() {

	}
}