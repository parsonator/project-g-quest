﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Entityicon : Icon {

    [SerializeField]
    Text nameText, hp;
    [SerializeField]

    Image pointIcon, focusIcon;

    [HideInInspector]

    public bool focus;
    [HideInInspector]
    public LivingEntity entity;
    Camera cam;

    [SerializeField] float fade = 1;
    [SerializeField] float fadeDis = 0.005f;
    [SerializeField] float maxDis = 500;
    [SerializeField] float dis;
    public void SetIcon(LivingEntity entity) {
        this.entity = entity;
        cam = Camera.main;
        target = entity.transform;
        nameText.text = string.Format("{0} {1} {2}",
            entity.data.firstName,
            entity.data.middleName,
            entity.data.lastName);
        hp.text = entity.totalHealth + "HP";
    }

    void Update() {
        float h = entity.totalHealth;
        hp.text = h + "HP";

        dis = Vector3.Distance(target.position, PlayerEntity.main.transform.position);
        display = (dis < maxDis || focus);
        fade = (activeTrack || focus)? 1 : 0;

        // if (h <= 10) hp.color = Color.red;
        // else if (h <= 50) hp.color = Color.yellow;
        hp.color = entity.GetHealthColor();
        hp.CrossFadeAlpha(fade, 0, false);
        nameText.CrossFadeAlpha(fade, 0, false);
        pointIcon.CrossFadeAlpha(fade, 0, false);

        float fFade = focus ? 1 : 0;
        focusIcon.CrossFadeAlpha(fFade, 0, false);

        // pointIcon.enabled = !offscreen();

    }
}