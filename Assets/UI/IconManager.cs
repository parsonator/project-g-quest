﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IconManager : MonoBehaviour {
    public GameObject entityIconFab;
    public GameObject targetIconFab;
    List<Entityicon> allIcons = new List<Entityicon>();
    // Use this for initialization
    void Start() {
        for (int i = 0; i < WorldData.GetWorldLivingEntitys().Count; i++) {
            Entityicon uiIcon = Instantiate(entityIconFab,transform).GetComponent<Entityicon>();
            uiIcon.SetIcon(WorldData.GetWorldLivingEntitys()[i]);
            allIcons.Add(uiIcon);
        }
        LockOnIcon lockIcon = Instantiate(targetIconFab,transform).GetComponent<LockOnIcon>();
        lockIcon.SetIcon(null);
        // allIcons.Add(lockIcon);
    }

    void Update() {
        if (GQuestGen.curruntMission != null) {
            List<LivingEntity> e = new List<LivingEntity>();
            if (!GQuestGen.curruntMission.EndNode()) {
                foreach (var ob in GQuestGen.curruntMission.outEdges) {
                    if (ob.objective == null)continue;
                    e.Add(ob.objective.curruntObjective.target);
                }

                foreach (var item in allIcons) {
                    item.focus = e.Contains(item.entity);
                }
            }
        }
    }

}