using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialougeButton : MonoBehaviour {
    [SerializeField] Button button;
    [SerializeField] Text text;
    MessageOptions options;
    void Awake() {

    }

    public void SetUpButton(MessageOptions options) {
        text = GetComponentInChildren<Text>();
        button = GetComponentInChildren<Button>();

        this.options = options;
        button.onClick.AddListener(onClick);
        text.text = options.optionMessage;
    }

    void onClick() {
        options.optionSelected();
    }
}