using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
public class EntityCreator : EditorWindow {
    GameObject gameObject;
    Editor gameObjectEditor;

    [MenuItem("Window/Entity Creator")]
    static void ShowWindow() {
        GetWindowWithRect<EntityCreator>(new Rect(0, 0, 256, 256));
    }

    void OnGUI() {
        gameObject = (GameObject) EditorGUILayout.ObjectField(gameObject, typeof(GameObject), true);

        GUIStyle bgColor = new GUIStyle();
        bgColor.normal.background = EditorGUIUtility.whiteTexture;

        if(gameObject != null) {
            if(gameObjectEditor == null)
                gameObjectEditor = Editor.CreateEditor(gameObject);

            gameObjectEditor.OnInteractivePreviewGUI(GUILayoutUtility.GetRect(position.width, position.height), bgColor);
        }
    }

}