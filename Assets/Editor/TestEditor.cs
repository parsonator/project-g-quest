using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Test))]
public class TestEditor : Editor {
    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        Test myScript = (Test) target;
        if(GUILayout.Button("Build Object")) {
            myScript.Pass();
        }
    }
}