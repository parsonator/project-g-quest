 using UnityEditor;
 using UnityEngine;

 [CustomEditor(typeof(NPCEntity))]
 public class NPCEditor : Editor {
     private Editor _editor;

     public override void OnInspectorGUI() {

         NPCEntity npc = (NPCEntity) target;
         if(GUILayout.Button("SetUp Entity")) {
             npc.SetUpEntity();
         }
         // if(npc.data != null)
         // GUILayout.Label (string.Format("{0}, {1}, {2}\n{3}",npc.Data.firstName,npc.Data.middleName,npc.Data.lastName,npc.Data.race.name));
         DrawDefaultInspector();

     }
 }