using System.Collections;
using System.Collections.Generic;
using GQuest;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

// [CreateAssetMenu(fileName = "GMissionRecipe", menuName = "GQuest/MissionRecipe", order = 1)]
[CustomEditor(typeof(RelationsRecipe))]

public class RelationsRecipeEditor : Editor {
    private ReorderableList relationsList;
    RelationsRecipe relationsRecipe;
    SerializedProperty Quest;
    // SerializedProperty bitListProperty = sequenceProperty.FindPropertyRelative("bitList");
    SerializedProperty goalObjective_List;
    SerializedProperty goalObjectiveNum_int;
    SerializedProperty type_enum;

    private void OnEnable() {
        relationsRecipe = (RelationsRecipe) target;
        Quest = serializedObject.FindProperty("Quest");
        BuildRelationsList();
    }

    void BuildRelationsList() {

        //layout
        // (DataType | DATA/String | RelationStrengths | DataType | DATA/String)

        relationsList = new ReorderableList(serializedObject,
            serializedObject.FindProperty("abstractRelationConditions"),
            true, true, true, true);
        relationsList.elementHeight *= 1.5f;
        relationsList.drawElementCallback =
            (Rect rect, int index, bool isActive, bool isFocused) => {
                var element = relationsList.serializedProperty.GetArrayElementAtIndex(index);
                rect.y += relationsList.elementHeight / 4;
                float rw = (rect.width / 5);
                EditorGUI.PropertyField(
                    new Rect(rect.x, rect.y, rw / 2, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("firstPartyDataType"), GUIContent.none);

                EditorGUI.PropertyField(
                    new Rect(rect.x + (rw * 0.5f), rect.y, rw * 1.5f, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative((relationsRecipe.abstractRelationConditions[index].firstPartyDataType == DataType.Specify) ? "firstPartySpecifiedData" : "firstPartyDataName"), GUIContent.none);

                EditorGUI.PropertyField(
                    new Rect(rect.x + (rw * 2f), rect.y, rw, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("relation"), GUIContent.none);

                EditorGUI.PropertyField(
                    new Rect(rect.x + (rw * 3), rect.y, rw * 0.5f, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("secondPartyDataType"), GUIContent.none);

                EditorGUI.PropertyField(
                    new Rect(rect.x + (rw * 3.5f), rect.y, rw * 1.5f, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative((relationsRecipe.abstractRelationConditions[index].secondPartyDataType == DataType.Specify) ? "secondPartySpecifiedData" : "secondPartyDataName"), GUIContent.none);
            };

        relationsList.drawHeaderCallback = (Rect rect) => {
            // rect.height *= 2;
            EditorGUI.LabelField(rect, "Nodes", EditorStyles.boldLabel);
        };
    }

    List<string> usedTargets {
        get {
            List<string> output = new List<string>();
            foreach(var arc in relationsRecipe.abstractRelationConditions) {
                if(arc.secondPartyDataType != DataType.Specify)
                    output.Add(arc.secondPartyDataName);
                else if(arc.secondPartySpecifiedData != null) {
                    output.Add(arc.secondPartySpecifiedData.name);
                }
            }
            output.Add("#");

            return output;
        }
    }

    void DrawObjectiveConstructor() {
        serializedObject.Update();
        EditorGUILayout.LabelField("Objective Constructor", EditorStyles.boldLabel);
        goalObjective_List = Quest.FindPropertyRelative("goalObjective");
        goalObjectiveNum_int = Quest.FindPropertyRelative("goalObjectiveNum");

        goalObjective_List.FindPropertyRelative("Array.size").intValue = goalObjectiveNum_int.intValue;
        int size = goalObjective_List.FindPropertyRelative("Array.size").intValue;

        EditorGUIUtility.labelWidth = 2;

        // GUILayout.FlexibleSpace();
        for(int i = 0; i < size; ++i) {
            SerializedProperty currunt_Object = goalObjective_List.GetArrayElementAtIndex(i);
            EditorGUILayout.LabelField("Objective " + i);

            EditorGUILayout.BeginHorizontal();

            int f = EditorGUILayout.Popup(currunt_Object.FindPropertyRelative("targetNo").intValue, usedTargets.ToArray());
            currunt_Object.FindPropertyRelative("targetNo").intValue = f;
            string target = usedTargets[f];

            currunt_Object.FindPropertyRelative("target").stringValue = target;

            type_enum = currunt_Object.FindPropertyRelative("type");
            GQGoal.MissionType type = (GQGoal.MissionType) type_enum.enumValueIndex;
            GQGoal.MissionType t = (GQGoal.MissionType) EditorGUILayout.EnumPopup("type", type);

            type_enum.enumValueIndex = (int) t;
            EditorGUILayout.EndHorizontal();
            DrawMissionMessage("MessageQueue", "MessageQueueNum", currunt_Object);

        }
        EditorGUILayout.Space();

        EditorGUILayout.BeginHorizontal();
        if(GUILayout.Button("Add objective")) {
            goalObjectiveNum_int.intValue++;
        }
        if(GUILayout.Button("Delete objective")) {
            goalObjectiveNum_int.intValue = Mathf.Clamp(--goalObjectiveNum_int.intValue, 0, 16);
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

    }

    void DrawMissionMessage(string ListName, string ListNumberName, SerializedProperty currunt_Object = null) {

        SerializedProperty MessageQueue_List;
        SerializedProperty MessageQueueNum_int;

        if(currunt_Object != null) {
            MessageQueue_List = currunt_Object.FindPropertyRelative(ListName);
            MessageQueueNum_int = currunt_Object.FindPropertyRelative(ListNumberName);
        } else {
            MessageQueue_List = serializedObject.FindProperty(ListName);
            MessageQueueNum_int = serializedObject.FindProperty(ListNumberName);
        }
        int messageSize = MessageQueue_List.FindPropertyRelative("Array.size").intValue;

        for(int m = 0; m < messageSize; ++m) {
            EditorGUILayout.LabelField("Message " + m);

            SerializedProperty currunt_message = MessageQueue_List.GetArrayElementAtIndex(m);
            string s = currunt_message.FindPropertyRelative("message").stringValue;
            currunt_message.FindPropertyRelative("message").stringValue = EditorGUILayout.TextArea(s, GUILayout.Height(EditorGUIUtility.singleLineHeight * 2));
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Message Target");

            int curM = EditorGUILayout.Popup(currunt_message.FindPropertyRelative("dataNameNo").intValue, usedTargets.ToArray());
            currunt_message.FindPropertyRelative("dataNameNo").intValue = curM;
            string msTarget = usedTargets[curM];

            currunt_message.FindPropertyRelative("dataName").stringValue = msTarget;
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();
        }
        // EditorGUILayout.Space();

        EditorGUILayout.BeginHorizontal();
        if(GUILayout.Button("Add message")) {
            MessageQueueNum_int.intValue++;
        }
        EditorGUILayout.Space();

        if(GUILayout.Button("delete message")) {
            MessageQueueNum_int.intValue = Mathf.Clamp(--MessageQueueNum_int.intValue, 0, 16);
        }
        EditorGUILayout.EndHorizontal();

        MessageQueue_List.FindPropertyRelative("Array.size").intValue = MessageQueueNum_int.intValue;
        serializedObject.ApplyModifiedProperties();

    }

    public override void OnInspectorGUI() {
        serializedObject.Update();
        relationsList.DoLayoutList();
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

        EditorGUILayout.LabelField("On Start Message Queue", EditorStyles.boldLabel);
        DrawMissionMessage("OnStartMessageQueue", "OnStartMessageQueueNum");
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

        EditorGUILayout.LabelField("On End Message Queue", EditorStyles.boldLabel);
        DrawMissionMessage("OnEndMessageQueue", "OnEndMessageQueueNum");
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

        DrawObjectiveConstructor();
        serializedObject.ApplyModifiedProperties();

        DrawDefaultInspector();

    }
}