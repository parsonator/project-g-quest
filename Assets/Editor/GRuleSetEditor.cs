using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GQuest;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor(typeof(GRuleSet))]
public class GRuleSetEditor : Editor {
    private Editor _editor;

    private ReorderableList relationsList;

    //leftRule
    private ReorderableList leftRuleNodes;
    private ReorderableList leftRuleEdges;

    //rightRule
    private ReorderableList rightRuleNodes;
    private ReorderableList rightRuleEdges;
    // private ReorderableList rightMissionMessages;

    SerializedProperty leftProductionRules;
    SerializedProperty rightProductionRules;

    GRuleSet gRuleSet;

    private void OnEnable() {

        gRuleSet = (GRuleSet) target;

        leftProductionRules = serializedObject.FindProperty("leftProductionRules");
        rightProductionRules = serializedObject.FindProperty("rightProductionRules");
        // rightMissionMessages = serializedObject.FindProperty("rightProductionRules");

        leftRuleNodes = BuildNodesList(leftProductionRules);
        leftRuleEdges = BuildEdgeList(leftProductionRules);

        rightRuleNodes = BuildNodesList(rightProductionRules);
        rightRuleEdges = BuildEdgeList(rightProductionRules);

        BuildRelationsList();
    }
    void DrawMissionMessage(string ListName, string ListNumberName, SerializedProperty currunt_Object = null) {

        SerializedProperty MessageQueue_List;
        SerializedProperty MessageQueueNum_int;

        if(currunt_Object != null) {
            MessageQueue_List = currunt_Object.FindPropertyRelative(ListName);
            MessageQueueNum_int = currunt_Object.FindPropertyRelative(ListNumberName);
        } else {
            MessageQueue_List = serializedObject.FindProperty(ListName);
            MessageQueueNum_int = serializedObject.FindProperty(ListNumberName);
        }
        int messageSize = MessageQueue_List.FindPropertyRelative("Array.size").intValue;

        for(int m = 0; m < messageSize; ++m) {
            EditorGUILayout.LabelField("Message " + m);

            SerializedProperty currunt_message = MessageQueue_List.GetArrayElementAtIndex(m);
            string s = currunt_message.FindPropertyRelative("message").stringValue;
            currunt_message.FindPropertyRelative("message").stringValue = EditorGUILayout.TextArea(s, GUILayout.Height(EditorGUIUtility.singleLineHeight * 2));
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Message Target");

            int curM = EditorGUILayout.Popup(currunt_message.FindPropertyRelative("dataNameNo").intValue, usedTargets.ToArray());
            currunt_message.FindPropertyRelative("dataNameNo").intValue = curM;
            string msTarget = usedTargets[curM];

            currunt_message.FindPropertyRelative("dataName").stringValue = msTarget;
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();
        }
        // EditorGUILayout.Space();

        EditorGUILayout.BeginHorizontal();
        if(GUILayout.Button("Add message")) {
            MessageQueueNum_int.intValue++;
        }
        EditorGUILayout.Space();

        if(GUILayout.Button("delete message")) {
            MessageQueueNum_int.intValue = Mathf.Clamp(--MessageQueueNum_int.intValue, 0, 16);
        }
        EditorGUILayout.EndHorizontal();

        MessageQueue_List.FindPropertyRelative("Array.size").intValue = MessageQueueNum_int.intValue;
        serializedObject.ApplyModifiedProperties();

    }

    void BuildRelationsList() {

        //layout
        // (DataType | DATA/String | RelationStrengths | DataType | DATA/String)

        relationsList = new ReorderableList(serializedObject,
            serializedObject.FindProperty("abstractRelationConditions"),
            true, true, true, true);
        relationsList.elementHeight *= 1.5f;
        relationsList.drawElementCallback =
            (Rect rect, int index, bool isActive, bool isFocused) => {
                var element = relationsList.serializedProperty.GetArrayElementAtIndex(index);
                // rect.y += relationsList.elementHeight / 4;
                float rw = (rect.width / 5);
                EditorGUI.PropertyField(
                    new Rect(rect.x, rect.y, rw / 2, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("firstPartyDataType"), GUIContent.none);

                EditorGUI.PropertyField(
                    new Rect(rect.x + (rw * 0.5f), rect.y, rw * 1.5f, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative((gRuleSet.abstractRelationConditions[index].firstPartyDataType == DataType.Specify) ? "firstPartySpecifiedData" : "firstPartyDataName"), GUIContent.none);

                EditorGUI.PropertyField(
                    new Rect(rect.x + (rw * 2f), rect.y, rw, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("relation"), GUIContent.none);

                EditorGUI.PropertyField(
                    new Rect(rect.x + (rw * 3), rect.y, rw * 0.5f, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("secondPartyDataType"), GUIContent.none);

                EditorGUI.PropertyField(
                    new Rect(rect.x + (rw * 3.5f), rect.y, rw * 1.5f, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative((gRuleSet.abstractRelationConditions[index].secondPartyDataType == DataType.Specify) ? "secondPartySpecifiedData" : "secondPartyDataName"), GUIContent.none);
            };

        relationsList.drawHeaderCallback = (Rect rect) => {
            // rect.height *= 2;
            EditorGUI.LabelField(rect, "Nodes", EditorStyles.boldLabel);
        };
    }

    int desplayMessageAt = 0;

    ReorderableList BuildEdgeList(SerializedProperty _GrathEdge) {
        ReorderableList _RuleNodesLists;
        //layout
        // (String | int)

        _RuleNodesLists = new ReorderableList(_GrathEdge.serializedObject,
            _GrathEdge.FindPropertyRelative("GrathEdges"),
            true, true, true, true);
        _RuleNodesLists.drawElementCallback =
            (Rect rect, int index, bool isActive, bool isFocused) => {
                var element = _RuleNodesLists.serializedProperty.GetArrayElementAtIndex(index);
                // rect.y += _RuleNodesLists.elementHeight/4;
                float rw = (rect.width / 2);
                EditorGUI.LabelField(new Rect(rect.x, rect.y, rw, EditorGUIUtility.singleLineHeight),
                    "source node ID");

                EditorGUI.PropertyField(
                    new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight, rw, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("source"), GUIContent.none);

                EditorGUI.LabelField(new Rect(rect.x + (rw * 1), rect.y, rw, EditorGUIUtility.singleLineHeight),
                    "target node ID");

                EditorGUI.PropertyField(
                    new Rect(rect.x + (rw * 1), rect.y + EditorGUIUtility.singleLineHeight, rw, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("target"), GUIContent.none);

                //objective constructor
                float objectiveY = EditorGUIUtility.singleLineHeight * 3;
                float messageY = EditorGUIUtility.singleLineHeight * 4;
                float objectiveLabelY = EditorGUIUtility.singleLineHeight * 2;
                float objRw = (rect.width / 4);

                SerializedProperty currunt_Object = element.FindPropertyRelative("objectiveConstructor.goalObjective");
                EditorGUI.LabelField(
                    new Rect(rect.x + (objRw * 0), rect.y + objectiveLabelY, objRw, EditorGUIUtility.singleLineHeight),
                    "Mission type");

                int f = EditorGUI.Popup(
                    new Rect(rect.x + (objRw * 0), rect.y + objectiveY, objRw, EditorGUIUtility.singleLineHeight),
                    currunt_Object.FindPropertyRelative("targetNo").intValue, usedTargets.ToArray());
                currunt_Object.FindPropertyRelative("targetNo").intValue = f;
                string target = usedTargets[f];
                currunt_Object.FindPropertyRelative("target").stringValue = target;

                EditorGUI.LabelField(new Rect(rect.x + (objRw * 1), rect.y + objectiveLabelY, objRw, EditorGUIUtility.singleLineHeight),
                    "Mission type");

                EditorGUI.PropertyField(
                    new Rect(rect.x + (objRw * 1), rect.y + objectiveY, objRw, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("objectiveConstructor.goalObjective.type"), GUIContent.none);

                EditorGUI.LabelField(new Rect(rect.x + (objRw * 2), rect.y + objectiveLabelY, objRw, EditorGUIUtility.singleLineHeight),
                    "Message count");

                EditorGUI.PropertyField(
                    new Rect(rect.x + (objRw * 2), rect.y + objectiveY, objRw, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("objectiveConstructor.goalObjective.MessageQueueNum"), GUIContent.none);

                SerializedProperty MessageQueue = element.FindPropertyRelative("objectiveConstructor.goalObjective.MessageQueue");
                SerializedProperty MessageQueueNum = element.FindPropertyRelative("objectiveConstructor.goalObjective.MessageQueueNum");
                SerializedProperty displayMessageAt = element.FindPropertyRelative("objectiveConstructor.goalObjective.displayMessageAt");

                MessageQueue.FindPropertyRelative("Array.size").intValue = MessageQueueNum.intValue;
                int size = MessageQueue.FindPropertyRelative("Array.size").intValue;

                displayMessageAt.intValue = Mathf.Clamp(displayMessageAt.intValue, 0, size - 1);

                EditorGUI.PropertyField(
                    new Rect(rect.x + (objRw * 0), rect.y + messageY, objRw * 0.5f, EditorGUIUtility.singleLineHeight),
                    displayMessageAt, GUIContent.none);

                if(size != 0) {
                    SerializedProperty MissionMessage = MessageQueue.GetArrayElementAtIndex(displayMessageAt.intValue);
                    if(MissionMessage != null) {
                        EditorGUI.PropertyField(
                            new Rect(rect.x + (objRw * 1), rect.y + messageY, objRw * 2, EditorGUIUtility.singleLineHeight * 2),
                            MissionMessage.FindPropertyRelative("message"), GUIContent.none);
                    }

                    int mt = EditorGUI.Popup(
                        new Rect(rect.x + (objRw * 0), rect.y + messageY + EditorGUIUtility.singleLineHeight, objRw * 0.5f, EditorGUIUtility.singleLineHeight),
                        MissionMessage.FindPropertyRelative("dataNameNo").intValue, usedTargets.ToArray());
                    MissionMessage.FindPropertyRelative("dataNameNo").intValue = mt;

                    string mTarget = usedTargets[mt];
                    MissionMessage.FindPropertyRelative("dataName").stringValue = mTarget;

                }
                // EditorGUI.pro(
                //     new Rect(rect.x+(objRw*1) , rect.y+objectiveY, objRw, EditorGUIUtility.singleLineHeight),
                //     element.FindPropertyRelative("objectiveConstructor.goalObjective.type"), GUIContent.none);
            };
        _RuleNodesLists.drawHeaderCallback = (Rect rect) => {
            // rect.height *= 2;
            EditorGUI.LabelField(rect, "Edge", EditorStyles.boldLabel);
        };

        return _RuleNodesLists;
    }
    ReorderableList BuildMessageList(SerializedProperty _GrathEdge) {
        ReorderableList _RuleNodesLists;
        //layout
        // (String | int)

        _RuleNodesLists = new ReorderableList(_GrathEdge.serializedObject,
            _GrathEdge.FindPropertyRelative("GrathEdges"),
            true, true, true, true);
        _RuleNodesLists.drawElementCallback =
            (Rect rect, int index, bool isActive, bool isFocused) => {
                var element = _RuleNodesLists.serializedProperty.GetArrayElementAtIndex(index);
                // rect.y += _RuleNodesLists.elementHeight/4;
                float rw = (rect.width / 2);
                EditorGUI.LabelField(new Rect(rect.x, rect.y, rw, EditorGUIUtility.singleLineHeight),
                    "source node ID");

                EditorGUI.PropertyField(
                    new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight, rw, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("source"), GUIContent.none);

                EditorGUI.LabelField(new Rect(rect.x + (rw * 1), rect.y, rw, EditorGUIUtility.singleLineHeight),
                    "target node ID");

                EditorGUI.PropertyField(
                    new Rect(rect.x + (rw * 1), rect.y + EditorGUIUtility.singleLineHeight, rw, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("target"), GUIContent.none);

                //objective constructor
                float objectiveY = EditorGUIUtility.singleLineHeight * 3;
                float objectiveLabelY = EditorGUIUtility.singleLineHeight * 2;
                float objRw = (rect.width / 4);

                SerializedProperty currunt_Object = element.FindPropertyRelative("objectiveConstructor.goalObjective");
                EditorGUI.LabelField(
                    new Rect(rect.x + (objRw * 0), rect.y + objectiveLabelY, objRw, EditorGUIUtility.singleLineHeight),
                    "Mission type");

                int f = EditorGUI.Popup(
                    new Rect(rect.x + (objRw * 0), rect.y + objectiveY, objRw, EditorGUIUtility.singleLineHeight),
                    currunt_Object.FindPropertyRelative("targetNo").intValue, usedTargets.ToArray());
                currunt_Object.FindPropertyRelative("targetNo").intValue = f;
                string target = usedTargets[f];
                currunt_Object.FindPropertyRelative("target").stringValue = target;

                EditorGUI.LabelField(new Rect(rect.x + (objRw * 1), rect.y + objectiveLabelY, objRw, EditorGUIUtility.singleLineHeight),
                    "Mission type");

                EditorGUI.PropertyField(
                    new Rect(rect.x + (objRw * 1), rect.y + objectiveY, objRw, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("objectiveConstructor.goalObjective.type"), GUIContent.none);

                // EditorGUI.pro(
                //     new Rect(rect.x+(objRw*1) , rect.y+objectiveY, objRw, EditorGUIUtility.singleLineHeight),
                //     element.FindPropertyRelative("objectiveConstructor.goalObjective.type"), GUIContent.none);
            };
        _RuleNodesLists.drawHeaderCallback = (Rect rect) => {
            // rect.height *= 2;
            EditorGUI.LabelField(rect, "Edge", EditorStyles.boldLabel);
        };

        return _RuleNodesLists;
    }
    ReorderableList BuildNodesList(SerializedProperty _grathRule) {
        ReorderableList _RuleNodes;
        //layout
        // (String | int)

        _RuleNodes = new ReorderableList(_grathRule.serializedObject,
            _grathRule.FindPropertyRelative("GrathNodes"),
            true, true, true, true);
        _RuleNodes.elementHeight *= 2;
        _RuleNodes.drawElementCallback =
            (Rect rect, int index, bool isActive, bool isFocused) => {
                var element = _RuleNodes.serializedProperty.GetArrayElementAtIndex(index);
                float rw = (rect.width / 2);
                EditorGUI.LabelField(new Rect(rect.x, rect.y, rw, EditorGUIUtility.singleLineHeight), "Name: node NO " + (index + 1));
                EditorGUI.PropertyField(
                    new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight, rw, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("name"), GUIContent.none);

                EditorGUI.LabelField(new Rect(rect.x + (rw * 1), rect.y, rw, EditorGUIUtility.singleLineHeight), "rule ID");
                EditorGUI.PropertyField(
                    new Rect(rect.x + (rw * 1), rect.y + EditorGUIUtility.singleLineHeight, rw, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("ruleID"), GUIContent.none);
            };
        _RuleNodes.drawHeaderCallback = (Rect rect) => {
            // rect.height *= 2;
            EditorGUI.LabelField(rect, "Nodes", EditorStyles.boldLabel);
        };

        return _RuleNodes;
    }

    List<string> usedTargets {
        get {
            List<string> output = new List<string>();

            foreach(var arc in gRuleSet.abstractRelationConditions) {

                if(arc.secondPartyDataType != DataType.Specify)
                    output.Add(arc.secondPartyDataName);
                else if(arc.secondPartySpecifiedData != null) {
                    output.Add(arc.secondPartySpecifiedData.name);
                }

                if(arc.firstPartyDataType != DataType.Specify)
                    output.Add(arc.firstPartyDataName);
                else if(arc.firstPartySpecifiedData != null) {
                    output.Add(arc.firstPartySpecifiedData.name);
                }

            }
            output.Add("#");

            return output;
        }
    }

    public override void OnInspectorGUI() {

        if(GUILayout.Button("view left production rules ")) {
            GGraph graph = gRuleSet.GenRule(gRuleSet.leftProductionRules);
            GNode s = graph.FindStartNode();
            s.Traverse(s, 0, true);
            NodeBasedEditor.DrawGrath(graph);
        }
        if(GUILayout.Button("view right production rules ")) {
            GGraph graph = gRuleSet.GenRule(gRuleSet.rightProductionRules);
            GNode startNode = graph.FindStartNode();
            startNode.Traverse(startNode, 0, true);
            NodeBasedEditor.DrawGrath(graph);
        }

        serializedObject.Update();

        EditorGUILayout.LabelField("For the Quest Giver use: # ");
        relationsList.DoLayoutList();
        EditorGUILayout.LabelField("Rules", EditorStyles.boldLabel);

        EditorGUILayout.LabelField("Left Side Rule", EditorStyles.boldLabel);

        leftRuleEdges.elementHeight = (leftRuleEdges.count == 0) ? EditorGUIUtility.singleLineHeight * 1 : EditorGUIUtility.singleLineHeight * 7f;

        leftRuleNodes.DoLayoutList();
        leftRuleEdges.DoLayoutList();

        EditorGUILayout.LabelField("Right Side Rule", EditorStyles.boldLabel);
        rightRuleEdges.elementHeight = (rightRuleEdges.count == 0) ? EditorGUIUtility.singleLineHeight * 1 : EditorGUIUtility.singleLineHeight * 7;

        rightRuleNodes.DoLayoutList();
        rightRuleEdges.DoLayoutList();
        // serializedObject.Update();

        // rightRuleNodes.DoLayoutList();

        EditorGUILayout.LabelField("On Start Message Queue", EditorStyles.boldLabel);
        DrawMissionMessage("OnStartMessageQueue", "OnStartMessageQueueNum");
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

        EditorGUILayout.LabelField("On End Message Queue", EditorStyles.boldLabel);
        DrawMissionMessage("OnEndMessageQueue", "OnEndMessageQueueNum");
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

        serializedObject.ApplyModifiedProperties();
        leftProductionRules.serializedObject.ApplyModifiedProperties();
        rightProductionRules.serializedObject.ApplyModifiedProperties();

        // DrawDefaultInspector();

    }
}