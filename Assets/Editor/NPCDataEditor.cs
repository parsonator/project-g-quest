using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor(typeof(LivingEntityData))]
public class NPCDataEditor : Editor {
    private ReorderableList bacelist;
    private ReorderableList activelist;

    private void drawList(ReorderableList list) {
        // list = new ReorderableList(serializedObject,
        //         serializedObject.FindProperty(name),
        //         true, true, true, true);
        list.drawElementCallback =
            (Rect rect, int index, bool isActive, bool isFocused) => {
                var element = list.serializedProperty.GetArrayElementAtIndex(index);
                EditorGUI.PropertyField(
                    new Rect(rect.x, rect.y, 100, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("gRelationStrength"), GUIContent.none);
                EditorGUI.PropertyField(
                    new Rect(rect.x + 100, rect.y, rect.width - 100, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("trait"), GUIContent.none);

            };

        list.drawHeaderCallback = (Rect rect) => {
            EditorGUI.LabelField(rect, "Relations");
        };
    }

    private void OnEnable() {
        bacelist = new ReorderableList(serializedObject,
            serializedObject.FindProperty("baseRelations"),
            true, true, true, true);

        drawList(bacelist);
        // drawList(activelist);
    }

    public override void OnInspectorGUI() {
        LivingEntityData npc = (LivingEntityData) target;

        DrawDefaultInspector();
        serializedObject.Update();

        bacelist.DoLayoutList();
        // activelist.DoLayoutList();
        serializedObject.ApplyModifiedProperties();
        // if (GUILayout.Button("Genorate Data"))
        // {
        //     npc.GenorateData();
        // }     
        if(GUILayout.Button("Genorate Relations")) {
            npc.GenorateRelations();
        }
        // if (GUILayout.Button("Genorate Traits"))
        // {
        //     npc.GenorateTraits();
        // }

    }
}