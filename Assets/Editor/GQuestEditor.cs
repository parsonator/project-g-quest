using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GQuestGen))]
public class GQuestEditor : Editor {
    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        GQuestGen gQuest = (GQuestGen) target;
        if(GUILayout.Button("Build gen")) {
            gQuest.ReplaceNodes(WorldData.RandomLivingWorldNPC());
            // NodeBasedEditor.DrawGrath(gQuest.CurruntGrath);
        }
        if(GUILayout.Button("Build gen & show")) {
            gQuest.ReplaceNodes(WorldData.RandomLivingWorldNPC());
            NodeBasedEditor.DrawGrath(gQuest.CurruntGrath);
        }
        if(GUILayout.Button("Show currunt Grath")) {
            NodeBasedEditor.DrawGrath(gQuest.CurruntGrath);
        }
    }
}