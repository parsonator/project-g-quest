using System.Collections.Generic;
using System.Linq;
using GQuest;
using UnityEditor;
using UnityEngine;
public class GraphRuleEditor : EditorWindow {
    public static GraphRuleEditor window;

    public GRuleSet curruntRule;

    [MenuItem("Window/Graph Rule Editor")]
    private static void OpenWindow() {
        window = GetWindow<GraphRuleEditor>();
        window.titleContent = new GUIContent("Graph Rule Editor");
        window.SetUp();
    }

    public void Awake() {
        SetUp();
    }

    public static void DrawGrath(GGraph graph) {

    }

    private void SetUp() {
        window = this;

    }

    int index = 0;

    float lw = EditorGUIUtility.singleLineHeight;

    private void OnGUI() {
        curruntRule = AllGRuleSets[index];
        curruntRule = (GRuleSet) EditorGUI.ObjectField(new Rect(3, 3, position.width - 6, 20), "Find Dependency", curruntRule, typeof(GRuleSet));
        // EditorGUI.ObjectField(new Rect(0, lw*3, position.width / 2, 20),serializedObject.FindProperty("curruntRule"))
        EditorGUI.LabelField(new Rect(0, lw * 0, position.width / 2, 20),
            "currunt Rule",
            curruntRule.name,
            EditorStyles.boldLabel
        );

        index = EditorGUI.Popup(
            new Rect(0, lw * 1, position.width / 2, lw),
            index,
            AllGRuleSetsNames.ToArray());

        this.Repaint();
    }

    List<GRuleSet> AllGRuleSets {
        get {
            List<GRuleSet> output = new List<GRuleSet>();
            output.AddRange(Resources.FindObjectsOfTypeAll(typeof(GRuleSet)) as GRuleSet[]);
            return output;
        }
    }
    List<string> AllGRuleSetsNames {
        get {
            List<string> output = new List<string>();
            foreach(var item in AllGRuleSets)
                output.Add(item.name);
            return output;
        }
    }

}