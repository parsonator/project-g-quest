using System.Collections.Generic;
using System.Linq;
using GQuest;
using UnityEditor;
using UnityEngine;
public class NodeBasedEditor : EditorWindow {
    public static NodeBasedEditor window;

    private List<Node> nodes;
    private List<Connection> connections;

    private GUIStyle nodeStyle;
    private GUIStyle selectedNodeStyle;
    private GUIStyle inPointStyle;
    private GUIStyle outPointStyle;

    private ConnectionPoint selectedInPoint;
    private ConnectionPoint selectedOutPoint;

    private Vector2 offset;
    private Vector2 drag;

    [MenuItem("Window/Node Based Editor")]
    private static void OpenWindow() {
        NodeBasedEditor window = (NodeBasedEditor) EditorWindow.GetWindow(typeof(NodeBasedEditor));
        window.Show();
        window.titleContent = new GUIContent("Node Based Editor");
        window.SetUp();
    }

    public void Awake() {
        SetUp();
    }

    public static void DrawGrath(GGraph graph) {
        NodeBasedEditor window = (NodeBasedEditor) EditorWindow.GetWindow(typeof(NodeBasedEditor));
        window.Show();
        NodeBasedEditor.window.nodes = new List<Node>();
        NodeBasedEditor.window.connections = new List<Connection>();
        int i = 0;
        GNode gn = (GNode) graph.Nodes.ToList() [0];
        // gn.outEdges[0].targetNode;
        GNode curruntNode = gn;
        List<GNode> nodes = graph.Nodes.ToList();
        nodes.OrderBy(o => o.id).ToList();
        foreach(GNode node in nodes) {
            Node n = new Node(new Vector2(500 + node.orderNo * 400, 100 + node.level * (200)), 200, 100, NodeBasedEditor.window.nodeStyle, NodeBasedEditor.window.selectedNodeStyle, NodeBasedEditor.window.inPointStyle, NodeBasedEditor.window.outPointStyle, NodeBasedEditor.window.OnClickInPoint, NodeBasedEditor.window.OnClickOutPoint, NodeBasedEditor.window.OnClickRemoveNode);
            i++;
            n.title = node.nodeSymbol;

            foreach(var ob in node.outEdges) {

                if(ob.objective != null) {
                    string msg = "\n" + ob.objective.questInfo;
                    foreach(var item in ob.objective.gObjectiveMeta.NPCmap) {
                        msg += item.Key;
                        msg += " --> ";
                        msg += item.Value.data.fullName;
                        msg += "\n";
                    }
                    n.connected += msg;
                }
            }

            n.id = node.id;
            foreach(GEdge inEdge in node.inEdges)
                n.inPoints.Add(inEdge.targetNode.id);

            foreach(GEdge outEdge in node.outEdges)
                n.outPoints.Add(outEdge.targetNode.id);
            // n.connected = connectedNodes;

            NodeBasedEditor.window.nodes.Add(n);
        }

        foreach(Node Anode in NodeBasedEditor.window.nodes) {
            foreach(Node Bnode in NodeBasedEditor.window.nodes) {
                foreach(string Iconect in Anode.inPoints) {
                    foreach(string Oconect in Bnode.outPoints) {
                        if(Iconect == Oconect)
                            NodeBasedEditor.window.connections.Add(new Connection(Anode.inPoint, Bnode.outPoint, NodeBasedEditor.window.OnClickRemoveConnection));
                    }
                }
            }
        }

    }

    public void RelaxNodes(List<Node> _nodes) {
        bool _separated;
        do {
            _separated = true;

            foreach(var room in _nodes) {

                // reset for iteration
                Vector2 velocity = new Vector2();
                Vector2 center = room.rect.center;

                foreach(var other_room in _nodes) {
                    if(room == other_room)
                        continue;

                    if(!room.rect.Overlaps(other_room.rect))
                        continue;

                    Vector2 other_center = other_room.rect.center;
                    Vector2 diff = new Vector2(center.x - other_center.x, center.y - other_center.y);
                    float diff_len2 = diff.sqrMagnitude;

                    if(diff_len2 > 0f) {
                        float repelDecayCoefficient = 1.0f;
                        float scale = repelDecayCoefficient / diff_len2;
                        diff.Normalize();
                        diff *= scale;

                        velocity += diff;
                    }
                }

                if(velocity.sqrMagnitude > 0f) {
                    _separated = false;

                    velocity = velocity.normalized * (Time.deltaTime * 20f);

                    room.rect.position += (velocity);
                }
            }
        } while (!_separated);
    }

    // public void Traverse(Node root)
    // {
    //     // Console.WriteLine(root.name);
    //     for (int i = 0; i < root.Employees.Count; i++)
    //     {
    //         Traverse(root.Employees[i]);
    //     }
    // }

    private void SetUp() {
        window = this;

        nodeStyle = GUI.skin.window;

        selectedNodeStyle = GUI.skin.window;

        inPointStyle = GUI.skin.window;

        outPointStyle = GUI.skin.window;

    }
    private void OnGUI() {
        SetUp();
        DrawGrid(20, 0.2f, Color.gray);
        DrawGrid(100, 0.4f, Color.gray);

        DrawNodes();
        DrawConnections();

        DrawConnectionLine(Event.current);

        ProcessNodeEvents(Event.current);
        ProcessEvents(Event.current);

        if(GQuestGen.curruntMission != null &&
            nodes != null) {
            foreach(var node in nodes) {
                node.isCurrunt = (node.id == GQuestGen.curruntMission.id);
            }
        }

        Repaint();
    }

    private void DrawGrid(float gridSpacing, float gridOpacity, Color gridColor) {
        int widthDivs = Mathf.CeilToInt(position.width / gridSpacing);
        int heightDivs = Mathf.CeilToInt(position.height / gridSpacing);

        Handles.BeginGUI();
        Handles.color = new Color(gridColor.r, gridColor.g, gridColor.b, gridOpacity);

        offset += drag * 0.5f;
        Vector3 newOffset = new Vector3(offset.x % gridSpacing, offset.y % gridSpacing, 0);

        for(int i = 0; i < widthDivs; i++) {
            Handles.DrawLine(new Vector3(gridSpacing * i, -gridSpacing, 0) + newOffset, new Vector3(gridSpacing * i, position.height, 0f) + newOffset);
        }

        for(int j = 0; j < heightDivs; j++) {
            Handles.DrawLine(new Vector3(-gridSpacing, gridSpacing * j, 0) + newOffset, new Vector3(position.width, gridSpacing * j, 0f) + newOffset);
        }

        Handles.color = Color.white;
        Handles.EndGUI();
    }

    private void DrawNodes() {
        if(nodes != null) {
            for(int i = 0; i < nodes.Count; i++) {
                nodes[i].Draw();
            }
        }
    }

    private void DrawConnections() {
        if(connections != null) {
            for(int i = 0; i < connections.Count; i++) {
                connections[i].Draw();
            }
        }
    }

    private void ProcessEvents(Event e) {
        drag = Vector2.zero;

        switch (e.type) {
            case EventType.MouseDown:
                if(e.button == 0) {
                    ClearConnectionSelection();
                }

                if(e.button == 1) {
                    ProcessContextMenu(e.mousePosition);
                }
                break;

            case EventType.MouseDrag:
                if(e.button == 0) {
                    OnDrag(e.delta);
                }
                break;
        }
    }

    private void ProcessNodeEvents(Event e) {
        if(nodes != null) {
            for(int i = nodes.Count - 1; i >= 0; i--) {
                bool guiChanged = nodes[i].ProcessEvents(e);

                if(guiChanged) {
                    GUI.changed = true;
                }
            }
        }
    }

    private void DrawConnectionLine(Event e) {
        if(selectedInPoint != null && selectedOutPoint == null) {
            Handles.DrawBezier(
                selectedInPoint.rect.center,
                e.mousePosition,
                selectedInPoint.rect.center + Vector2.left * 50f,
                e.mousePosition - Vector2.left * 50f,
                Color.white,
                null,
                2f
            );

            GUI.changed = true;
        }

        if(selectedOutPoint != null && selectedInPoint == null) {
            Handles.DrawBezier(
                selectedOutPoint.rect.center,
                e.mousePosition,
                selectedOutPoint.rect.center - Vector2.left * 50f,
                e.mousePosition + Vector2.left * 50f,
                Color.white,
                null,
                2f
            );

            GUI.changed = true;
        }
    }

    private void ProcessContextMenu(Vector2 mousePosition) {
        GenericMenu genericMenu = new GenericMenu();
        genericMenu.AddItem(new GUIContent("Add node"), false, () => OnClickAddNode(mousePosition));
        genericMenu.ShowAsContext();
    }

    private void OnDrag(Vector2 delta) {
        drag = delta;

        if(nodes != null) {
            for(int i = 0; i < nodes.Count; i++) {
                nodes[i].Drag(delta);
            }
        }

        GUI.changed = true;
    }

    private void OnClickAddNode(Vector2 mousePosition) {
        if(nodes == null) {
            nodes = new List<Node>();
        }

        nodes.Add(new Node(mousePosition, 200, 50, nodeStyle, selectedNodeStyle, inPointStyle, outPointStyle, OnClickInPoint, OnClickOutPoint, OnClickRemoveNode));
    }

    private void OnClickInPoint(ConnectionPoint inPoint) {
        selectedInPoint = inPoint;

        if(selectedOutPoint != null) {
            if(selectedOutPoint.node != selectedInPoint.node) {
                CreateConnection();
                ClearConnectionSelection();
            } else {
                ClearConnectionSelection();
            }
        }
    }

    private void OnClickOutPoint(ConnectionPoint outPoint) {
        selectedOutPoint = outPoint;

        if(selectedInPoint != null) {
            if(selectedOutPoint.node != selectedInPoint.node) {
                CreateConnection();
                ClearConnectionSelection();
            } else {
                ClearConnectionSelection();
            }
        }
    }

    private void OnClickRemoveNode(Node node) {
        if(connections != null) {
            List<Connection> connectionsToRemove = new List<Connection>();

            for(int i = 0; i < connections.Count; i++) {
                if(connections[i].inPoint == node.inPoint || connections[i].outPoint == node.outPoint) {
                    connectionsToRemove.Add(connections[i]);
                }
            }

            for(int i = 0; i < connectionsToRemove.Count; i++) {
                connections.Remove(connectionsToRemove[i]);
            }

            connectionsToRemove = null;
        }

        nodes.Remove(node);
    }

    private void OnClickRemoveConnection(Connection connection) {
        connections.Remove(connection);
    }

    private void CreateConnection() {
        if(connections == null) {
            connections = new List<Connection>();
        }

        connections.Add(new Connection(selectedInPoint, selectedOutPoint, OnClickRemoveConnection));
    }

    private void ClearConnectionSelection() {
        selectedInPoint = null;
        selectedOutPoint = null;
    }
}