using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
public class TraitsData_Test {

    [Test]
    public void GetLevelChain_Test() {
        TraitsData tempData = ScriptableObject.CreateInstance<TraitsData>();
        List<TraitsData> allData = TraitsData.allTraitsData();

        TraitsData Knowledge = allData.Find(x => x.name.ToLower() == "knowledge");

        // Debug.LogFormat("TraitsData_Test/GetLevelChain_Test:No of   {0}",Knowledge.GetLevelChain().Count);

        Assert.True(Knowledge.GetLevelChain().Count == 4);

    }

    // [Test]
    // public void AddTraits_Test() {
    //     TraitsData tempData = ScriptableObject.CreateInstance<TraitsData>();
    //     List<TraitsData> allData = TraitsData.allTraitsData();

    //     TraitsData wise = allData.Find(x => x.name.ToLower() == "wise");
    //     TraitsData knowledge = allData.Find(x => x.name.ToLower() == "knowledge");

    //     tempData.AddTrait(knowledge);
    //     tempData.AddTrait(knowledge);
    //     tempData.AddTrait(knowledge);

    //     Assert.True(tempData.ActiveListOfTraits.Contains(wise));
    //     Assert.True(tempData.ActiveListOfTraits.Count == 1);

    // }

    // [Test]
    // public void RemoveTraits_Test() {
    //     TraitsData tempData = ScriptableObject.CreateInstance<TraitsData>();
    //     List<TraitsData> allData = TraitsData.allTraitsData();

    //     TraitsData wise = allData.Find(x => x.name.ToLower() == "wise");
    //     TraitsData smart = allData.Find(x => x.name.ToLower() == "smart");
    //     TraitsData knowledge = allData.Find(x => x.name.ToLower() == "knowledge");

    //     tempData.AddTrait(knowledge);
    //     tempData.AddTrait(knowledge);
    //     tempData.AddTrait(knowledge);

    //     Assert.True(tempData.ActiveListOfTraits.Contains(wise));
    //     Assert.True(tempData.ActiveListOfTraits.Count == 1);

    //     tempData.RemoveTrait(knowledge);

    //     Assert.True(tempData.ActiveListOfTraits.Contains(smart));
    //     Assert.True(tempData.ActiveListOfTraits.Count == 1);

    // }

    // [Test]
    // public void HasTraits_Test() {
    //     TraitsData tempData = ScriptableObject.CreateInstance<TraitsData>();
    //     List<TraitsData> allData = TraitsData.allTraitsData();

    //     TraitsData wise = allData.Find(x => x.name.ToLower() == "wise");
    //     TraitsData smart = allData.Find(x => x.name.ToLower() == "smart");
    //     TraitsData knowledge = allData.Find(x => x.name.ToLower() == "knowledge");

    //     tempData.AddTrait(knowledge);
    //     tempData.AddTrait(knowledge);
    //     // tempData.AddTrait(knowledge);

    //     Assert.True(tempData.ActiveListOfTraits.Contains(smart));
    //     Assert.True(tempData.ActiveListOfTraits.Count == 1);

    //     Assert.True(tempData.HasTraits(smart));
    //     Assert.True(tempData.HasTraits(knowledge));
    //     Assert.False(tempData.HasTraits(wise));

    // }
}