using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;

[ExecuteInEditMode]
public class Test : MonoBehaviour {

    public TextAsset Grammer;
    public List<Symbol> Symbols;

    // Use this for initialization
    public void Pass() {
        JSONNode N = JSON.Parse(Grammer.text);
        string a = "";

        for(int i = 0; i < N["Start"].Count; i++) {
            string type = N["Start"][i];
            string tmp = "";
            for(int b = 0; b < N.Count; b++) {
                int selection = Random.Range(0, N[b].Count);
                // print( string.Format("{0} vs {1} : selection = {2}",N[b], N[type],selection));
                if(N[b] == N[type]) {
                    // print(selection +" on pass "+ b);
                    tmp += N[b][selection];
                    tmp += " ";
                    break;
                }

            }
            if(tmp == "") {
                tmp += type;
                tmp += " ";

            }
            a += tmp;
        }
        print(a);

    }

}