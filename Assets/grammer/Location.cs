using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Location : MonoBehaviour {

    public List<KeyLocation> locations;

    private void OnDrawGizmos() {
        Gizmos.color = Color.red;

        foreach(KeyLocation location in locations) {
            RaycastHit hit;
            //Ray r = new Ray(location.getCoordinates(), Vector3.down);
            Physics.Raycast(location.getCoordinates() + Vector3.up * 1000, Vector3.down, out hit, 1000);
            Gizmos.DrawCube(hit.point, Vector3.one * 10);
        }
    }

    // Ray mouseRay = HandleUtility.GUIPointToWorldRay(guiEvent.mousePosition);
    // RaycastHit hit;
    // float drawPlaneHeight = 0;
    // float dstToDrawPlane = (drawPlaneHeight - mouseRay.origin.y) / mouseRay.direction.y;
    // Vector3 mousePosition = mouseRay.GetPoint(dstToDrawPlane);   

}

[System.Serializable]
public struct KeyLocation {
    public string name;
    public Vector2 coordinates;

    public Vector3 getCoordinates() {
        return new Vector3(coordinates.x, 0, coordinates.y);
    }
}