using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// [CreateAssetMenu(fileName = "Unit", menuName = "grammer/Unit", order = 1)]
public class unity : ScriptableObject {
    //bio;
    public string unitName;
    public int healthPoints = 100;
    public int staminaPoints = 150;
    public int money;

}