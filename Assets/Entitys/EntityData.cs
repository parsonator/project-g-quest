using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;

[CreateAssetMenu(fileName = "GNPCData", menuName = "GQuest/NPC/NPCDATA", order = 1)]

public class EntityData : Data {

    public string firstName = "";
    public string middleName = "";
    public string lastName = "";
    public int age;
    public float baceHealth = 0;

    [Header("Looks")]
    public Looks looks;

    public string fullName {
        get {
            List<string> name = new List<string>();
            if(firstName != "") name.Add(firstName);
            if(middleName != "") name.Add(middleName);
            if(lastName != "") name.Add(lastName);

            string output = "";
            for(int i = 0; i < name.Count; i++) {
                output += UppercaseFirst(name[i].Trim());
                if(i != name.Count) output += " ";
            }
            return output;
        }
    }

    protected string UppercaseFirst(string s) {
        // Check for empty string.
        if(string.IsNullOrEmpty(s)) {
            return string.Empty;
        }
        // Return char and concat substring.
        return char.ToUpper(s[0]) + s.Substring(1);
    }

    [System.Serializable]
    public class Looks {
        public GameObject entityModel;
        public GameObject attackEffect;
        public Sprite Icon;
        public RuntimeAnimatorController animator;
    }

}