using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Party {

    public static List<Party> Parties = new List<Party>();
    public string partyID;
    public List<LivingEntity> partyMembers = new List<LivingEntity>();

    public Party() {
        Party.Parties.Add(this);
    }

    public bool AddToParty(LivingEntity entity) {
        if(!partyMembers.Contains(entity)) {
            entity.party = this;
            partyMembers.Add(entity);
            return true;
        }
        return false;
    }

    public void LeaveParty(LivingEntity entity) {
        partyMembers.Remove(entity);
        entity.party = null;
    }

    public void DisbandParty() {
        foreach(LivingEntity partyMember in partyMembers) {
            LeaveParty(partyMember);
        }
        Parties.Remove(this);
    }

    // public static void AddToNPCParty(Entity entity, Entity entityToJoin)
    // {
    // 	if(entityToJoin.Party == null)
    // 	{
    // 		Party p =  new Party();
    // 		entityToJoin.Party = p;
    // 	}
    // 	if(!entityToJoin.Party.partyMembers.Contains(entity))
    // 	{
    // 		if(entity.Party) leaveParty(entity);
    // 		entityToJoin.Party.addToParty(entity);

    // }

}