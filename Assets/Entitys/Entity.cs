using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(AudioSource))]
public abstract class Entity : MonoBehaviour {

    [Header("Entity")]
    public EntityData baceData;
    public EntityData data {
        get { return baceData; }
    }
    public List<TraitsData> ActiveListOfTraits = new List<TraitsData>();

    [HideInInspector]
    public EntityMotion motion;
    [HideInInspector]
    public Animator animator;
    [HideInInspector]
    public AudioSource audioSource;
    [HideInInspector]
    public Collider collider;
    public List<Modifier.ModifierInsurance> appliedModifiers = new List<Modifier.ModifierInsurance>();
    public float totalHealth = 100;
    public float startHealth = 100;
    public float damage = 0;

    protected Entity lastHitFrom;

    protected AnimationEvents animationEvents;
    private Transform mesh;
    private int EntityLayer {
        get {
            return LayerMask.GetMask("Entity");
        }
    }
    #region Constructors
    public Entity() {}
    public Entity(EntityData _entityData) {
        baceData = _entityData;
    }
    #endregion
    #region Entity life Functions
    public virtual void Revive() {
        totalHealth = startHealth;
        if(motion != null) {
            motion.blockMotion = false;
            motion.ResetMotion();
        }
        if(animator) animator.SetTrigger("Restart");
    }

    public virtual bool isDead(bool animate = false) {
        bool ded = (totalHealth <= 0);
        if(animate && animator) animator.SetBool("Dead", ded);
        return ded;
    }

    public virtual void Damage(float _damage, Entity _attacker) {
        if(animator) animator.SetTrigger("Hit");
        totalHealth -= _damage;
        lastHitFrom = _attacker;
        isDead(true);
    }
    #endregion
    #region Trait functions
    public void SetUpTraits() {
        List<TraitsData> Traits = data.baselistOfTraits;
        ActiveListOfTraits = new List<TraitsData>();
        foreach(var item in Traits) {
            TraitsData d = AddTrait(item);
            if(item.LifeTime != 0) {
                StartCoroutine(TimedTrait(d));
            }
        }
    }

    public TraitsData AddTrait(TraitsData _trait) {
        TraitsData addedData = _trait;
        foreach(var item in ActiveListOfTraits) {
            if(item.blockTrait.Contains(_trait)) return addedData;
            if(addedData.TraitInLevel(item)) {

                addedData = item.GetNextLevel();
                if(addedData != item) {
                    RemoveModifiers(item.AllModifiers());
                    ActiveListOfTraits.Remove(item);

                    ApplyModifiers(addedData.AllModifiers());
                    ActiveListOfTraits.Add(addedData);

                    if(addedData.LifeTime != 0) {
                        StartCoroutine(TimedTrait(_trait));
                    }
                }
                if(data.OnTraitChange != null) data.OnTraitChange();
                return addedData;
            }
        }
        ApplyModifiers(addedData.AllModifiers());
        ActiveListOfTraits.Add(addedData);

        if(addedData.LifeTime != 0) {
            StartCoroutine(TimedTrait(_trait));
        }
        if(data.OnTraitChange != null) data.OnTraitChange();
        return addedData;

    }

    public void RemoveTrait(TraitsData _trait) {
        TraitsData RemovedData = _trait;
        foreach(var item in ActiveListOfTraits) {
            if(RemovedData.TraitInLevel(item)) {
                RemovedData = item.GetNextLevel();
                if(RemovedData != _trait) {
                    RemoveModifiers(item.AllModifiers());
                    ActiveListOfTraits.Remove(item);

                    ApplyModifiers(RemovedData.AllModifiers());
                    ActiveListOfTraits.Add(RemovedData);

                    if(RemovedData.LifeTime != 0) {
                        StartCoroutine(TimedTrait(_trait));
                    }
                } else {
                    RemoveModifiers(RemovedData.AllModifiers());
                    ActiveListOfTraits.Remove(RemovedData);
                }
                if(data.OnTraitChange != null) data.OnTraitChange();
                return;
            }
        }
        if(data.OnTraitChange != null) data.OnTraitChange();
    }

    public void CompileTraits() {
        List<TraitsData> ATraits = new List<TraitsData>();

        foreach(var a in ActiveListOfTraits) {
            bool safe = true;
            foreach(var b in ActiveListOfTraits) {
                if(b.blockTrait.Contains(b)) {
                    safe = false;
                    RemoveTrait(a);
                    break;
                }
            }
        }
    }

    [SerializeField]
    List<TraitsData> TimedTraits = new List<TraitsData>();
    public IEnumerator TimedTrait(TraitsData _trait) {
        if(TimedTraits.Contains(_trait)) yield break;
        TimedTraits.Add(_trait);
        yield return new WaitForSeconds(_trait.LifeTime);
        RemoveTrait(_trait);
        TimedTraits.Remove(_trait);

    }

    public virtual bool HasTraits(TraitsData _trait) {

        foreach(var item in ActiveListOfTraits) {
            var levels = item.GetLevelChain();
            if(levels.ContainsValue(_trait)) {
                foreach(var x in levels)
                    if(x.Key > _trait.GetTraitLevel()) return true;
            }
        }
        return false;
    }

    public virtual bool HasData(Data _data) {

        if(this.data == _data) {
            return true;
        } else if(_data as TraitsData) {
            return ActiveListOfTraits.Contains(_data as TraitsData);
        }
        return false;
    }
    #endregion
    #region Modifier functions
    public void ApplyModifiers(List<Modifier> _modifiers) {
        foreach(var modifier in _modifiers) {
            Modifier.ModifierInsurance mi = new Modifier.ModifierInsurance();
            mi.appliedModifier = StartCoroutine(modifier.ApplyModifier(this));
            mi.modifier = modifier;
            appliedModifiers.Add(mi);
        }

    }

    public void RemoveModifiers(List<Modifier> _modifiers) {
        foreach(var modifier in _modifiers) {
            Modifier.ModifierInsurance mi = appliedModifiers.Find(x => x.modifier == modifier);
            if(mi != null) {
                StopCoroutine(mi.appliedModifier);
                StartCoroutine(mi.modifier.RemoveModifier(this));
                appliedModifiers.Remove(mi);
            }
        }
    }
    public virtual List<Modifier> AllModifiers() {
        List<Modifier> modifiers = new List<Modifier>();
        // modifiers.AddRange(this.modifiers);
        if(ActiveListOfTraits.Any())
            foreach(var t in ActiveListOfTraits) {
                modifiers.AddRange(t.Modifiers);
                modifiers.AddRange(t.AllModifiers());
            }
        return modifiers;
    }
    #endregion
    #region Entity functions
    public virtual void SetUpEntity() {

        mesh = transform.Find("Mesh");
        if(mesh == null) {
            mesh = new GameObject("Mesh").transform;
            mesh.transform.localPosition = Vector3.zero;
        }

        if(!(animator = mesh.GetComponent<Animator>())) animator = Instantiate<Animator>(new Animator(), mesh);
        if(!(animationEvents = mesh.GetComponent<AnimationEvents>())) animationEvents = Instantiate<AnimationEvents>(new AnimationEvents(), mesh);

        if(!(audioSource = GetComponent<AudioSource>())) audioSource = Instantiate<AudioSource>(new AudioSource(), transform);
        if(!(motion = GetComponent<EntityMotion>())) {
            if(this is NPCEntity) motion = Instantiate<NPCMotion>(new NPCMotion(), transform);
            if(this is PlayerMovement) motion = Instantiate<PlayerMotion>(new PlayerMotion(), transform);
            // if (this is ObjectEntity)   motion = Instantiate<>(new NPCMotion(),transform);   
        }

        if(data) {
            if(this is NPCEntity) gameObject.name = "(NPC) " + data.name;
            if(this is PlayerEntity) gameObject.name = "(Player) " + data.name;
            if(this is ObjectEntity) gameObject.name = "(Object) " + data.name;
            startHealth = data.baceHealth;
        }
        Revive();
    }

    public virtual void PlaceLooks() {
        AnimationEvents ee;
        ee = GetComponentInChildren<AnimationEvents>();

        for(int i = 0; i < mesh.childCount; i++) {
            Destroy(mesh.GetChild(i).gameObject);
        }

        animator = GetComponentInChildren<Animator>();
        if(animator && data.looks.animator) animator.runtimeAnimatorController = data.looks.animator;

        // if (data.looks.attackEffect)ee.attackParticleSystem = Instantiate(data.looks.attackEffect, mesh).GetComponent<ParticleSystem>();

        GameObject go = Instantiate(data.looks.entityModel, mesh);
        go.transform.localPosition = Vector3.zero;
    }

    public void Freeze(bool _freeze = true) {
        animator.enabled = !_freeze;
        collider.enabled = !_freeze;
        audioSource.enabled = !_freeze;

        motion.rigidbody.isKinematic = _freeze;
        motion.navMeshAgent.enabled = !_freeze;
        motion.enabled = !_freeze;

        this.enabled = !_freeze;
    }
    #endregion
    #region Unity Functions
    protected virtual void Awake() {
        SetUpEntity();
        PlaceLooks();
    }

    protected virtual void Start() {
        SetUpTraits();
    }

    #endregion
    #region Click Functions
    public virtual void OnClick() {}

    #endregion
    #region Helper Functions

    public List<Entity> GetEntitiesInRadius(float _radius) {
        List<Collider> cols = new List<Collider>(Physics.OverlapSphere(transform.position, _radius, EntityLayer));
        List<Entity> Es = new List<Entity>();

        foreach(var col in cols) {
            Entity e;
            if(e = col.GetComponentInParent<Entity>())
                Es.Add(e);
        }
        return Es;
    }

    public List<LivingEntity> GetLivingEntitiesInRadius(float _radius) {
        List<LivingEntity> Es = new List<LivingEntity>();
        List<Entity> Ls = GetEntitiesInRadius(_radius);

        foreach(var liv in Ls) {
            LivingEntity Le = liv as LivingEntity;
            if(Le) Es.Add(Le);
        }

        return Es;
    }

    public Color GetHealthColor() {
        Color c = Color.white;
        if(totalHealth <= startHealth / 4) c = Color.red;
        else if(totalHealth <= startHealth / 2) c = Color.yellow;
        else c = Color.white;
        return c;
    }

    public static Entity Spawn(Entity _entityData, Vector3 _position) {
        Entity e = Instantiate(_entityData, _position, _entityData.transform.rotation);
        return e;
    }
    //TODO: Add in Spawing form data
    // public static Entity Spawn(EntityData _entityData, Vector3 _position) {
    // }

    #endregion
    #region Editor
    void OnValidate() {
        // SetUpEntity();
        if(Application.isPlaying) return;
        if(data == null) {
            Debug.LogError("Warning: No data on: " + gameObject.name);
            return;
        }

        this.name = data.name + "_(Entity)";
        mesh = transform.Find("Mesh");
        for(int i = 0; i < mesh.childCount; i++) {
            GameObject child = mesh.GetChild(i).gameObject;
            if(Application.isEditor)
                UnityEditor.EditorApplication.delayCall += () => {
                    DestroyImmediate(child.gameObject, true);
                };
        }
        GameObject go = Instantiate(data.looks.entityModel, mesh);
        go.transform.localPosition = Vector3.zero;
    }
    #endregion

}