using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "GTraitsData", menuName = "GQuest/Traits/Trait", order = 1)]
public class TraitsData : Data {
    public TraitsData superiorTrait;
    public TraitsData inferiorTrait;
    public List<Modifier> Modifiers = new List<Modifier>();
    public float LifeTime;
    public List<TraitsData> blockTrait = new List<TraitsData>();

    public TraitsData Clone() {
        TraitsData t = ScriptableObject.CreateInstance<TraitsData>();
        t.name = this.name;
        t.superiorTrait = this.superiorTrait;
        t.inferiorTrait = this.inferiorTrait;
        t.Modifiers = this.Modifiers;
        t.LifeTime = this.LifeTime;
        t.blockTrait = this.blockTrait;
        return t;
    }

    public Dictionary<int, TraitsData> GetLevelChain() {
        Dictionary<int, TraitsData> allTraits = new Dictionary<int, TraitsData>();
        int level = 0;
        // TraitsData t = GetBaceTrait();
        TraitsData targ = GetBaceTrait();
        while (targ != null) {
            allTraits.Add(level, targ);
            targ = targ.superiorTrait;
            level++;
        }
        return allTraits;
    }

    public TraitsData GetNextLevel() {
        if(superiorTrait != null) {
            return superiorTrait;
        }
        return this;
    }
    public TraitsData GetLastLevel() {
        if(inferiorTrait != null) {
            return inferiorTrait;
        }
        return this;
    }

    // public void Distruct()
    // {
    //     distroy
    // }

    public bool TraitInLevel(TraitsData _trait) {
        return GetLevelChain().ContainsValue(_trait);
    }

    public override List<Modifier> AllModifiers() {
        List<Modifier> modifiers = base.AllModifiers();
        modifiers.AddRange(this.Modifiers);
        return modifiers;
    }

    /// <summary>
    /// Retruns the bace level of a the inferiorTrait chaine;
    /// </summary>
    /// <returns>returns the TraitData of the bace of the inferiorTrait chain</returns>
    public TraitsData GetBaceTrait() {
        TraitsData targ = this;
        while (targ.inferiorTrait != null) {
            targ = targ.inferiorTrait;
        }

        return targ;
    }

    public int GetTraitLevel() {
        int level = this.GetLevelChain().FirstOrDefault(x => x.Value == this).Key;
        return level;
    }

    /// <summary>
    /// find if a trait is in superior or inferior chain;
    /// </summary>
    /// <param name="_trait">the target trait</param>
    /// <returns>Returns true if trait is in superior or inferior line</returns>
    public bool IsRelatedTrait(TraitsData _trait) {
        TraitsData targ = this;
        while (inferiorTrait != null) {
            if(_trait == targ) {
                return true;
            }
            targ = targ.inferiorTrait;
        }
        while (superiorTrait != null) {
            if(_trait == targ) {
                return true;
            }
            targ = targ.superiorTrait;
        }
        return false;
    }

    public bool HasTraits(TraitsData _trait) {
        if(base.HasData(_trait)) return true;

        foreach(var t in baselistOfTraits) {
            if(t.HasTraits(_trait)) {
                return true;
            }
        }

        return false;
    }
    public static List<TraitsData> allTraitsData() {
        List<TraitsData> gr = new List<TraitsData>();
        gr.AddRange(Resources.FindObjectsOfTypeAll(typeof(TraitsData)) as TraitsData[]);
        return gr;
    }

    public class TraitDataInst {
        public TraitsData traitsData;
        public float timer;
        public bool active;

        public TraitDataInst(TraitsData _traitsData) {
            traitsData = _traitsData;
            timer = _traitsData.LifeTime;
            active = false;
        }

    }
}