using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GRaceData", menuName = "GQuest/Traits/Race", order = 1)]
public class RaceData : TraitsData {
    public static List<RaceData> allDataTypes {
        get {
            List<RaceData> gr = new List<RaceData>();
            gr.AddRange(Resources.FindObjectsOfTypeAll(typeof(RaceData)) as RaceData[]);
            return gr;
        }
    }
}