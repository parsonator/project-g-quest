using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Data : ScriptableObject {
    public Sprite icon;

    public List<TraitsData> baselistOfTraits = new List<TraitsData>();

    [HideInInspector]

    // public Data Clone()
    // {
    //     Data d = ScriptableObject.CreateInstance<Data>();
    //     d.name = this.name;
    //     d.ActivelistOfTraits = this.ActivelistOfTraits;
    //     return d;

    // }

    protected virtual void Awake() {
        // ActiveListOfTraits = new List < TraitsData > ();
        // ActiveListOfTraits.AddRange(BaselistOfTraits);
    }

    public delegate void onAddTrate();
    public onAddTrate OnTraitChange;

    // public TraitsData AddTrait(TraitsData _trait){
    //     TraitsData addedData = _trait;
    //     Dictionary<int,TraitsData> levels = _trait.GetLevelChain();
    //     if(!ActiveListOfTraits.Any()){
    //         ActiveListOfTraits.Add(addedData);
    //     }
    //     if(!ActiveListOfTraits.Contains(_trait))
    //     {
    //         ActiveListOfTraits.Add(addedData);
    //     }
    //     else
    //     foreach (var item in ActiveListOfTraits)
    //     {
    //         if(levels.ContainsValue(item)){
    //             int level = levels.FirstOrDefault(x => x.Value == item).Key;
    //             if(levels.Count-1 > level)
    //                 addedData = levels[level + 1];
    //             ActiveListOfTraits.Add(addedData);
    //             ActiveListOfTraits.Remove(item);
    //             break;
    //         }
    //     }

    //     if(OnTraitChange != null) OnTraitChange();
    //     return addedData;
    // }

    // public void RemoveTrait(TraitsData _trait){
    //     TraitsData addedData = _trait;
    //     Dictionary<int,TraitsData> levels = _trait.GetLevelChain();

    //     foreach (var item in ActiveListOfTraits)
    //     {
    //         if(levels.ContainsValue(item)){
    //             int level = item.GetTraitLevel();
    //             if(levels.Count-1 < level)
    //                 addedData = levels[level - 1];
    //             ActiveListOfTraits.Remove(item);
    //             break;
    //         }
    //         else if(addedData == item)
    //         {
    //             ActiveListOfTraits.Remove(item);
    //             break;
    //         }
    //     }
    //     if(OnTraitChange != null) OnTraitChange();

    // }

    public virtual List<Modifier> AllModifiers() {
        List<Modifier> modifiers = new List<Modifier>();
        // modifiers.AddRange(this.modifiers);
        if(baselistOfTraits.Any())
            foreach(var t in baselistOfTraits) {
                modifiers.AddRange(t.Modifiers);
                modifiers.AddRange(t.AllModifiers());
            }
        return modifiers;
    }

    public static List<Data> allDataTypes {
        get {
            List<Data> gr = new List<Data>();
            gr.AddRange(Resources.FindObjectsOfTypeAll(typeof(Data)) as Data[]);
            return gr;
        }
    }

    public virtual bool HasData(Data _data) {
        return this == _data;
    }

    public virtual bool HasTraits(TraitsData _trait) {

        foreach(var item in baselistOfTraits) {
            var levels = item.GetLevelChain();
            if(levels.ContainsValue(_trait)) {
                foreach(var x in levels)
                    if(x.Key > _trait.GetTraitLevel()) return true;
            }
        }
        return false;
    }

}