using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public abstract class LivingEntity : Entity {

    public List<NPCRelation> ActiveRelations = new List<NPCRelation>();
    public Party party;

    public Inventory inventory;

    // [HideInInspector] public Queue<Message> MissionToMessageQueue = new Queue<Message>();

    protected Vector3 attackPoz {
        get {
            return transform.position + (transform.forward / 2) + (transform.up);
        }
    }

    public LivingEntityData livingData {
        get {
            return data as LivingEntityData;
        }
    }

    #region Constructors
    public LivingEntity(LivingEntityData _LivingEntityData) : base(_LivingEntityData) {}
    #endregion
    #region Entity functions overides
    public List<Entity> Attack() {
        Vector3 direction = -transform.forward;
        animationEvents.PlayAttack();
        Debug.DrawLine(attackPoz, attackPoz + (direction), Color.red, 1);
        direction = Quaternion.AngleAxis(livingData.attackArc / 2, transform.up) * direction;

        float elevationAngle;
        List<Entity> hitEntitys = new List<Entity>();

        int segs = 30;

        for(int i = 0; i < segs; i++) {
            elevationAngle = livingData.attackArc / segs;
            direction = Quaternion.AngleAxis(elevationAngle, transform.up) * direction;
            Debug.DrawLine(attackPoz, attackPoz + (direction * livingData.attackRange), Color.gray, 1);

            RaycastHit hit;
            Entity hitEntity;
            Ray ray = new Ray(attackPoz, direction);
            if(Physics.Raycast(ray, out hit, livingData.attackRange) && (hitEntity = hit.transform.GetComponentInParent<Entity>())) {
                if(!hitEntitys.Contains(hitEntity) && hitEntity != this)
                    hitEntitys.Add(hitEntity);
            }
        }
        foreach(var item in hitEntitys) {
            item.Damage(damage, this);
            Debug.DrawLine(item.transform.position, item.transform.position + Vector3.up, Color.red, 1);
        }

        return null;
    }

    public override void Damage(float _damage, Entity _attacker) {
        base.Damage(_damage, _attacker);
        if(_attacker != this)
            AddRelations(_attacker.data, -25f);
    }
    #endregion
    #region NPCRelation functions

    public void AddRelations(Data _data, float _addStrength) {
        int relationIndex = ActiveRelations.FindIndex(x => x.trait == _data);

        if(relationIndex != -1) {
            ActiveRelations[relationIndex].gRelationStrength += _addStrength;
        } else {
            ActiveRelations.Add(new NPCRelation(_addStrength, _data));
        }
    }

    public RelationStrengths GetRelationStrength(Data _Data) {
        NPCRelation d = ActiveRelations.Find(x => x.trait == _Data);
        if(d != null) {
            return d.CurruntRelation();
        } else return RelationStrengths.NO_RELATION;

    }

    public bool HasRelationship(AbstractRelation _abstractRelation) {
        foreach(var relation in ActiveRelations) {
            List<NPCRelation> filtered = ActiveRelations.FindAll(x => x.CurruntRelation() == _abstractRelation.relation);

            if(filtered.Any())
                return true;
        }
        return false;
    }

    #endregion
    #region Entity functions
    public override void SetUpEntity() {

        base.SetUpEntity();
        if(!(collider = GetComponent<CapsuleCollider>())) {
            CapsuleCollider cc = Instantiate<CapsuleCollider>(new CapsuleCollider(), transform);
            cc.height = 1.5f;
            cc.center = new Vector3(0, 0.75f, 0);
            cc.radius = 0.5f;
            cc.direction = 1;
        }

        if(animationEvents && data.looks.attackEffect) animationEvents.ReplaceAttack(data.looks.attackEffect, attackPoz);
        if(livingData.race == null)
            Debug.LogError(livingData + " has no no race!");
        ActiveRelations = new List<NPCRelation>();
        ActiveRelations.AddRange(livingData.baseRelations);

    }
    public virtual void PlaceLooks() {
        base.PlaceLooks();
    }
    #endregion
    #region Data Functions
    public override bool HasData(Data _data) {
        if(base.HasData(_data))
            return true;

        var nPCData = _data as LivingEntityData;
        if(nPCData != null) return this == nPCData;
        var raceData = _data as RaceData;
        if(raceData != null) return (livingData.race == raceData);
        var traitData = _data as TraitsData;
        if(traitData != null)
            return base.HasTraits(traitData);
        return false;
    }
    #endregion
    #region Unity functions
    void Update() {
        motion.enabled = isDead(true);
    }
    void LateUpdate() {
        animator.SetBool("Walking", (motion.MovementVelocity().magnitude > 0.2f));
    }
    protected override void Awake() {
        base.Awake();
        animator = GetComponentInChildren<Animator>();
        motion = GetComponent<EntityMotion>();
    }
    #endregion
    #region Editor
    private void OnDrawGizmos() {

    }
    #endregion

}