using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AbstractRelation {
    public DataType firstPartyDataType;
    public string firstPartyDataName = "#";
    public Data firstPartySpecifiedData;

    public RelationStrengths relation;

    public DataType secondPartyDataType;
    public string secondPartyDataName;
    public Data secondPartySpecifiedData;

}
public enum DataType {
    NPCData,
    // ObjectData,
    TraitsData,
    RaceData,
    Data,
    Specify
}