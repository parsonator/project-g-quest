using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCEntity : LivingEntity {

    public delegate bool Event();

    public bool dead = true;

    public override void Revive() {
        motion.enabled = false;
        dead = true;
        base.Revive();

    }
    public override bool isDead(bool animate = false) {
        bool ded = base.isDead(animate);
        if(ded && Action != null) {
            StopCoroutine(Action);
        }

        if(dead != ded) {
            motion.enabled = !ded;
            dead = ded;
        }

        return ded;
    }

    private void Update() {
        List<LivingEntity> e = GetLivingEntitiesInRadius(5);
        if(e.Count > 0) {
            foreach(var entity in e) {
                if(entity.isDead() || entity == this) continue;
                RelationStrengths r = GetRelationStrength(entity.data);
                if(r == RelationStrengths.DISLIKE || r == RelationStrengths.HATE) {
                    AttackEntity(entity);
                    break;
                }
            }
        }

    }

    Coroutine Action;
    float lastAgilerRot;

    public NPCEntity(LivingEntityData _LivingEntityData) : base(_LivingEntityData) {}

    IEnumerator GoAttack(Entity _entity) {
        lastAgilerRot = motion.navMeshAgent.angularSpeed;

        (motion as NPCMotion).movePattern = NPCMotion.AIMovePattern.COMBATMODE;
        yield return MoveToRange(_entity);
        Attack();
        yield return new WaitForSeconds(1);

        while (!_entity.isDead()) {
            if(isDead()) break;

            int i = Random.Range(0, 100);
            if(i < 80)
                yield return GoAttack(_entity);
            else
                yield return StrafeAtRange(_entity, 4);
        }
        (motion as NPCMotion).movePattern = NPCMotion.AIMovePattern.WANDERONSPOT;
        motion.navMeshAgent.angularSpeed = lastAgilerRot;
    }

    #region AIPatterns
    IEnumerator MoveToRange(Entity entity) {
        motion.navMeshAgent.angularSpeed = 0;
        while (Vector3.Distance(transform.position, entity.transform.position) > livingData.attackRange) {
            motion.GoTo(entity.transform.position);
            motion.LookAt(entity.transform.position);
            if(isDead()) {
                (motion as NPCMotion).movePattern = NPCMotion.AIMovePattern.WANDERONSPOT;
                yield return null;
            }
            yield return new WaitForEndOfFrame();
        }
    }

    public void AttackEntity(Entity _entity) {
        Debug.LogFormat("{0} is attaking {1}", this, _entity);
        if(Action == null)
            Action = StartCoroutine(GoAttack(_entity));
    }
    IEnumerator StrafeAtRange(Entity _entity, float _dis) {

        float t = 0;
        float time = Random.Range(0.5f, 1.5f);
        float dis = Random.Range(_dis, _dis + 4);
        while (t < 2) {
            var heading = _entity.transform.position - transform.position;
            var poz = (heading / heading.magnitude).normalized * dis;
            motion.GoTo(_entity.transform.position - (heading / heading.magnitude).normalized * dis);
            motion.LookAt(_entity.transform.position);
            if(isDead()) {
                (motion as NPCMotion).movePattern = NPCMotion.AIMovePattern.WANDERONSPOT;
                yield return null;
            }
            t += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }

    #endregion

}