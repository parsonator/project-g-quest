using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCMotion : NavMeshMotion {

    public AIMovePattern movePattern;
    public float wanderDis = 4;
    public float wanderRate = 4;
    private Vector3 startPoint;

    // Use this for initialization
    void Awake() {
        base.Awake();

        startPoint = transform.position;
    }

    // Update is called once per frame
    void Update() {
        switch (movePattern) {
            case AIMovePattern.WANDERONSPOT:
                navMeshAgent.angularSpeed = 500;
                navMeshAgent.stoppingDistance = 0.1f;
                wander();
                break;
            case AIMovePattern.JOB:
                navMeshAgent.angularSpeed = 500;
                navMeshAgent.stoppingDistance = 0.1f;
                break;
            case AIMovePattern.FOLLOWPLAYER:
                navMeshAgent.angularSpeed = 500;
                GoTo(PlayerEntity.main.transform.position);
                navMeshAgent.stoppingDistance = 1.5f;
                break;
            case AIMovePattern.COMBATMODE:
                navMeshAgent.angularSpeed = 0;
                navMeshAgent.stoppingDistance = 0.1f;
                break;

            default:
                break;
        }
    }

    float tick;
    float wanderNo = 0;
    void wander() {
        tick += Time.deltaTime;
        if(tick > wanderNo) {
            wanderNo = Random.Range(1, wanderRate);
            GoTo(startPoint + (
                new Vector3(Random.Range(-wanderDis, wanderDis), 0, Random.Range(-wanderDis, wanderDis))));
            tick = 0;
        }
    }

    public override void ResetMotion() {
        movePattern = AIMovePattern.WANDERONSPOT;
    }

    // public override void Move(Vector3 _direction) {
    //     navMeshAgent.Move(_direction);
    // }

    // public override Vector3 MovementVelocity() {
    //     return navMeshAgent.velocity;
    // }
    public enum AIMovePattern {
        WANDERONSPOT,

        COMBATMODE,
        JOB,
        FOLLOWPLAYER
    }

}