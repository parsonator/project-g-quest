// using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GNPCData", menuName = "GQuest/NPC/NPCDATA", order = 1)]

public class LivingEntityData : EntityData {

    [Header("Data")]
    public float attackArc = 180;
    public float attackRange = 2;
    public List<Message> defaultTalkToMessage;
    public AttackType attackType;

    public RaceData race {
        get {
            return baselistOfTraits.Find(x => x as RaceData != null) as RaceData;
        }
    }

    [HideInInspector]
    public List<NPCRelation> baseRelations = new List<NPCRelation>();

    [Header("gQuest")]

    #region Generation functions
    public bool ExudeFromGenoration = false;

    public void GenorateData() {
        LivingEntityData newNPC = new LivingEntityData();
        TextAsset asset = Resources.Load("Names", typeof(TextAsset)) as TextAsset;
        string[] linesInFile = asset.text.Split('\n');
        firstName = linesInFile[Random.Range(0, linesInFile.Length)].ToLower();
        firstName.Trim();
        firstName = UppercaseFirst(firstName);

        lastName = linesInFile[Random.Range(0, linesInFile.Length)].ToLower();
        lastName.Trim();
        lastName = UppercaseFirst(lastName);

        age = Random.Range(18, 100);
    }
    public void GenorateRelations() {
        int len = Random.Range(1, 10);
        List<Data> data = new List<Data>();
        foreach(var item in baseRelations)
            data.Add(item.trait);

        for(int i = 0; i < len; i++) {
            NPCEntity e = WorldData.RandomWorldNPC();
            while ((baseRelations.Find(x => x.trait == e.data) != null) && e.data == this)
                e = WorldData.RandomWorldNPC();
            AddRelations(e.data, Random.Range(-100, 100));
        }

    }

    public void AddRelations(Data _data, float _addStrength) {
        int relationIndex = baseRelations.FindIndex(x => x.trait == _data);

        if(relationIndex != -1) {
            baseRelations[relationIndex].gRelationStrength += _addStrength;
        } else {
            baseRelations.Add(new NPCRelation(_addStrength, _data));
        }
    }

    #endregion

    public LivingEntityData Clone() {
        LivingEntityData led = ScriptableObject.CreateInstance<LivingEntityData>();
        led.name = this.name;
        led.icon = this.icon;
        led.baselistOfTraits = new List<TraitsData>(this.baselistOfTraits);
        led.firstName = this.firstName;
        led.middleName = this.middleName;
        led.lastName = this.lastName;
        led.age = this.age;
        led.looks = this.looks;

        led.attackType = this.attackType;
        led.baseRelations = new List<NPCRelation>(this.baseRelations);

        return led;

    }

    public override bool HasData(Data _data) {
        base.HasData(_data);
        var nPCData = _data as LivingEntityData;
        if(nPCData != null) return this == nPCData;

        var raceData = _data as RaceData;
        if(raceData != null) return (race == raceData);

        var traitData = _data as TraitsData;
        if(traitData != null) return base.HasTraits(traitData);

        return false;
    }

    public enum AttackType {
        RANGED,
        MELEE,
    }

}