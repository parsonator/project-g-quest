using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NPCRelation {

    public NPCRelation(float _gRelation, Data _Data) {
        gRelationStrength = _gRelation;
        trait = _Data;
    }
    public float gRelationStrength;
    public Data trait;

    public RelationStrengths CurruntRelation() {
        if(gRelationStrength > 80) return RelationStrengths.LOVE;
        else if(gRelationStrength >= 60 && gRelationStrength <= 80) return RelationStrengths.LIKE;
        else if(gRelationStrength >= -59 && gRelationStrength <= 59) return RelationStrengths.MUTUAL;
        else if(gRelationStrength >= -80 && gRelationStrength <= -60) return RelationStrengths.DISLIKE;
        else return RelationStrengths.HATE;
    }
}
public enum RelationStrengths {
    HATE,
    DISLIKE,
    MUTUAL,
    LIKE,
    LOVE,

    NO_RELATION
}