using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Rigidbody))]
public abstract class EntityMotion : MonoBehaviour {

    [HideInInspector]
    public NavMeshAgent navMeshAgent;

    [HideInInspector]
    public Rigidbody rigidbody;

    [HideInInspector]
    public CapsuleCollider capsuleCollider;

    public float speed = 10;
    public bool blockMotion = false;

    protected virtual void Awake() {
        if(!(navMeshAgent = GetComponent<NavMeshAgent>())) navMeshAgent = Instantiate<NavMeshAgent>(new NavMeshAgent(), transform);
        if(!(rigidbody = GetComponent<Rigidbody>())) rigidbody = Instantiate<Rigidbody>(new Rigidbody(), transform);
        capsuleCollider = GetComponent<CapsuleCollider>();
    }
    public virtual void LookAt(Vector3 _Poz) {
        // if (!blockMotion)
        transform.LookAt(_Poz);
    }
    public virtual void LookAt(Quaternion _direction) {
        // if (!blockMotion)
        transform.rotation = _direction;
    }

    public virtual IEnumerable Looking(Vector3 _direction) {
        if(!blockMotion)
            while (true) {
                LookAt(_direction);
                yield return new WaitForEndOfFrame();
            }
    }
    public abstract void GoTo(Vector3 _point);
    public abstract void ResetMotion();
    public abstract void Stop();
    public virtual bool ReachedDestination(float distence = 0.05f) {
        float distanceToTarget = Vector3.Distance(transform.position, navMeshAgent.destination);
        return (distanceToTarget < distence);
    }

    public virtual bool Grounded() {
        NavMeshHit hit;
        Vector3 downVec = transform.position;
        Vector3 startvec = transform.position - (Vector3.down * 0.1f);
        bool blocked = false;
        blocked = NavMesh.SamplePosition(downVec, out hit, 0.1f, NavMesh.AllAreas);

        return blocked;

    }
    public virtual void SetNav(bool _set) {
        navMeshAgent.enabled = _set;
        capsuleCollider.isTrigger = _set;
    }
    public virtual void Move(Vector3 _direction) {
        navMeshAgent.Move(_direction);
    }
    public abstract Vector3 MovementVelocity();

}