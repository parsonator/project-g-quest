using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ObjectEntity : Entity {
    public ObjectEntity(EntityData _entityData) : base(_entityData) {}

    Vector3 attackPoz {
        get {
            return transform.position + transform.forward + (transform.up / 2);
        }
    }

    public ObjectEntityData ObjectData {
        get {
            return data as ObjectEntityData;
        }
    }

}