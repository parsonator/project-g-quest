using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Modifier : ScriptableObject {

    public Sprite icon;
    public string description = "";
    public AudioClip soundEffect;
    public GameObject effect;
    [HideInInspector]
    public AudioSource source;

    public abstract IEnumerator ApplyModifier(Entity _effectedEntity);

    public abstract IEnumerator RemoveModifier(Entity _effectedEntity);

    [System.Serializable]
    public class ModifierInsurance {
        public Modifier modifier;
        public Coroutine appliedModifier;

    }

}