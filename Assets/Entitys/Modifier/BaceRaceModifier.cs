using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BaceRaceModifier", menuName = "Modifiers/BaceRaceModifier", order = 0)]
public class BaceRaceModifier : Modifier {

    public float startMaxHP = 100;
    public float startSpeed = 10;
    public float startDammage = 10;

    public override IEnumerator ApplyModifier(Entity _effectedEntity) {
        //mod
        _effectedEntity.totalHealth = startMaxHP;
        _effectedEntity.startHealth = startMaxHP;
        _effectedEntity.damage = startDammage;
        _effectedEntity.motion.speed += startSpeed;

        //effects
        yield return null;

    }

    public override IEnumerator RemoveModifier(Entity _effectedEntity) {
        _effectedEntity.totalHealth -= startMaxHP;
        _effectedEntity.startHealth -= startMaxHP;
        _effectedEntity.damage -= startDammage;
        _effectedEntity.motion.speed -= startSpeed;

        yield return null;

    }

}