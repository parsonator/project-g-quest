using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpeedModifier", menuName = "Modifiers/SpeedModifier", order = 0)]
public class SpeedModifier : Modifier {

    public float SpeedAmount = 10;

    public override IEnumerator ApplyModifier(Entity _effectedEntity) {
        //mod
        _effectedEntity.motion.speed += SpeedAmount;

        //setup
        if(effect) {

            if(soundEffect) _effectedEntity.GetComponent<AudioSource>().PlayOneShot(soundEffect);

            GameObject worldEffect = Instantiate(effect, _effectedEntity.transform.position, Quaternion.identity);
            worldEffect.transform.parent = _effectedEntity.transform;
            worldEffect.name = this.name;
            ParticleSystem particleEffect = worldEffect.GetComponent<ParticleSystem>();

            yield return new WaitForSeconds(particleEffect.duration);
            Destroy(worldEffect.gameObject);
        }
    }

    public override IEnumerator RemoveModifier(Entity _effectedEntity) {
        _effectedEntity.motion.speed -= SpeedAmount;
        yield return null;

    }

}