using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "HealthModifier", menuName = "Modifiers/HealthModifier", order = 0)]
public class HealthModifier : Modifier {

    public float MaxHPAmount = 10;
    ParticleSystem particleEffect;
    GameObject worldEffect;

    public override IEnumerator ApplyModifier(Entity _effectedEntity) {
        //setup
        worldEffect = Instantiate(effect, _effectedEntity.transform.position, Quaternion.identity);
        worldEffect.transform.parent = _effectedEntity.transform;
        particleEffect = worldEffect.GetComponent<ParticleSystem>();

        //mod
        _effectedEntity.totalHealth += MaxHPAmount;
        _effectedEntity.startHealth += MaxHPAmount;

        //effects
        particleEffect.Play();
        _effectedEntity.GetComponent<AudioSource>().PlayOneShot(soundEffect);
        yield return new WaitForSeconds(particleEffect.duration);
        Destroy(worldEffect);

    }

    public override IEnumerator RemoveModifier(Entity _effectedEntity) {
        _effectedEntity.startHealth -= MaxHPAmount;
        _effectedEntity.totalHealth -= MaxHPAmount;
        yield return null;

    }

}