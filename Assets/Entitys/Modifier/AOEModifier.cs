using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AOEModifier", menuName = "Modifiers/AOEModifier", order = 0)]
public class AOEModifier : Modifier {

    public float rate = 10;
    public float spreadRate = 10;
    public TraitsData spreadTraitsData;

    public override IEnumerator ApplyModifier(Entity _effectedEntity) {
        //setup
        GameObject worldEffect = Instantiate(effect, _effectedEntity.transform.position, Quaternion.identity);
        worldEffect.transform.parent = _effectedEntity.transform;
        worldEffect.name = this.name;
        yield return new WaitForEndOfFrame();

        ParticleSystem particleEffect = worldEffect.GetComponent<ParticleSystem>();
        if(particleEffect.shape.shapeType == ParticleSystemShapeType.MeshRenderer) {
            var sh = particleEffect.shape;
            MeshRenderer mr = _effectedEntity.GetComponentInChildren<MeshRenderer>();
            sh.meshRenderer = mr;
        }

        GameObject AOE = new GameObject(this.name + "_AOE");
        AOE.transform.parent = _effectedEntity.transform;
        AOE.transform.localPosition = Vector3.up;
        SphereCollider col = AOE.AddComponent<SphereCollider>();
        col.isTrigger = true;
        col.radius = 2;

        EffectTraitsOverTime eTOT = AOE.AddComponent<EffectTraitsOverTime>();
        eTOT.rate = spreadRate;
        eTOT.trait = spreadTraitsData;

        AudioSource source = _effectedEntity.GetComponent<AudioSource>();
        while (true) {
            if(particleEffect != null)
                particleEffect.Play();

            if(soundEffect != null);
            source.PlayOneShot(soundEffect);
            yield return new WaitForSeconds(rate);
        }
    }

    public override IEnumerator RemoveModifier(Entity _effectedEntity) {
        Transform worldEffect = _effectedEntity.transform.Find(this.name);
        ParticleSystem particleEffect;

        Destroy(_effectedEntity.transform.Find(this.name + "_AOE").gameObject);

        if(worldEffect) {
            particleEffect = worldEffect.GetComponent<ParticleSystem>();
            particleEffect.Stop();
            yield return new WaitForSeconds(particleEffect.duration);
        } else yield break;
        Destroy(worldEffect.gameObject);
    }

}