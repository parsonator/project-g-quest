using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : LivingEntity {
    // public PlayerMotion playerMotion;
    public static PlayerMovement main;
    public float actionDistence = 10;

    public CurruntAction curruntAction;
    Camera cam;

    // Use this for initialization
    protected override void Awake() {
        base.Awake();
        main = this;
    }

    public override void OnClick() {}

    // public override void EnterCombatMode(Vector3 _startPoint)
    // {
    //     motion.GoTo(_startPoint);
    // }

    void Start() {
        cam = Camera.main;
    }
    void Update() {
        RaycastHit hit = MapClickPoz();
        NPCEntity NPCHit = null;
        if(hit.transform != null)
            NPCHit = hit.transform.GetComponentInChildren<NPCEntity>();

        switch (GameState.curruntState) {
            case GameState.GameModeState.ADVENTURE: //over world
                if((NPCHit != null) && GameState.curruntState == GameState.GameModeState.ADVENTURE) {
                    if(Input.GetMouseButtonDown(0)) {
                        ClickEntity(NPCHit);
                    }
                } else if(Input.GetMouseButton(0)) {
                    motion.GoTo(hit.point);
                }
                break;

            case GameState.GameModeState.COMBAT: //combat 
                break;
        }

    }

    public void ClickEntity(NPCEntity _clickedNPC) {
        // if (Vector3.Distance(transform.position, _clickedNPC.transform.position) < actionDistence)
        // {

        //     Queue<Message> m = new Queue<Message>();

        //     Message intro = null;
        //     if (_clickedNPC.MissionToMessageQueue.Any())
        //     {
        //         m = _clickedNPC.MissionToMessageQueue;
        //         intro = m.ToArray()[m.Count - 1];
        //     }
        //     else
        //     {
        //         if (_clickedNPC.data.defaultTalkToMessage.Any())
        //         {
        //             m = new Queue<Message>(_clickedNPC.data.defaultTalkToMessage);
        //             intro = m.ToArray()[m.Count - 1];
        //         }
        //         else
        //         {
        //             intro = new Message("Hello, my name is " + _clickedNPC.data.firstName, (LivingEntity)_clickedNPC);
        //             m.Enqueue(intro);
        //             // .SetLastMessage();

        //         }
        //         intro.options = new List<MessageOptions>();
        //         intro.options.Add(new MessageOptions(
        //         delegate ()
        //         {
        //             DialogueSystem.StopTalking();
        //             GQuestGen.main.TestReplaceNodes(_clickedNPC);
        //         },
        //         "Start a new Quest?"));
        //         intro.SetLastMessage();

        //     }
        //     DialogueSystem.DisplayMessageQueue(m,(LivingEntity)_clickedNPC);
        //     motion.GoTo(_clickedNPC.transform.position);
        // }
        // else
        // {
        //     motion.GoTo(_clickedNPC.transform.position);
        // }
        // _clickedNPC.OnClick();
    }

    public int curruntActionInt;

    public PlayerMovement(LivingEntityData _LivingEntityData) : base(_LivingEntityData) {}

    public void UpdateState() {
        float v = Input.GetAxis("Mouse ScrollWheel");
        int actionsLength = System.Enum.GetNames(typeof(CurruntAction)).Length;
        if(v > 0f) {
            curruntActionInt += 1;
        } else if(v < 0f) {
            curruntActionInt -= 1;
        }

        if(curruntActionInt > actionsLength - 1) {
            curruntActionInt = 0;
        }
        if(curruntActionInt < 0) {
            curruntActionInt = actionsLength - 1;
        }

        curruntAction = (CurruntAction) curruntActionInt;
        MouseIcon.SetSpriteState(curruntAction);
    }

    RaycastHit MapClickPoz() {
        Vector3 movePoint;
        RaycastHit hit;

        Ray r = cam.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(r, out hit);
        return hit;
    }

}
public enum CurruntAction {
    TALK = 0,
    ATTACK = 1
}