using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cinemachine;
using UnityEngine;
using UnityEngine.AI;

public class PlayerEntity : LivingEntity {
    public static PlayerEntity main;

    [Header("Camrea")]
    public CinemachineTargetGroup targetCam;
    public CinemachineFreeLook normalRig;
    public CurruntAction curruntAction;
    Camera cam;

    // Use this for initialization
    protected override void Awake() {
        base.Awake();
        main = this;
    }

    // public override void EnterCombatMode(Vector3 _startPoint) {
    //     motion.GoTo(_startPoint); 
    // }
    Vector3 Focusdirection;

    [HideInInspector]
    public Entity eTarget = null;
    public bool eTargeting = false;

    protected override void Start() {
        base.Start();
        cam = Camera.main;
    }

    void Update() {
        Vector3 direction = InputManager.PlayerMovement();
        Vector3 camRelativeDirection = Camera.main.transform.forward;
        Vector3 newMoveDirection = new Vector3();
        newMoveDirection += Camera.main.transform.right.normalized * direction.x;
        newMoveDirection += Camera.main.transform.forward.normalized * direction.z;
        newMoveDirection.y = 0;
        motion.Move(newMoveDirection);

        if(PlayerInput.AimDown()) {
            TargetLock();
        }
        if(eTarget != null) {
            targetCam.m_Targets[1].target = eTarget.transform;
            Focusdirection = eTarget.transform.position - this.transform.position;
        } else {
            targetCam.m_Targets[1].target = targetCam.m_Targets[0].target;
        }

        if(eTargeting) {
            Focusdirection.y = 0;
            motion.LookAt(Quaternion.LookRotation(Focusdirection));
        } else {
            if(direction != Vector3.zero)
                motion.LookAt(Quaternion.LookRotation(newMoveDirection));
        }

        if(PlayerInput.AttackDown()) {
            Attack();
            animator.SetTrigger("Attack");
        }
        if(motion.Grounded()) {
            motion.SetNav(true);
            motion.rigidbody.velocity = Vector3.zero;
        }
        if(PlayerInput.InteractDown()) {
            Interact();
        }
    }

    public void Interact() {
        Collider[] collides = Physics.OverlapSphere(transform.position, 3, LayerMask.GetMask("Entity"));
        if(collides.Length > 0) {
            foreach(var eHit in collides) {
                NPCEntity e = eHit.GetComponentInParent<NPCEntity>();
                if(e == null) continue;
                ClickEntity(e);
            }
        }

    }

    public void ClickEntity(NPCEntity _clickedNPC) {
        Queue<Message> m = new Queue<Message>();
        Message intro = null;
        if(DialogueSystem.MissionMessages != null && DialogueSystem.MissionMessages.ContainsKey(_clickedNPC)) {
            m = DialogueSystem.MissionMessages[_clickedNPC];
            intro = m.ToArray() [m.Count - 1];
        } else {
            if(_clickedNPC.livingData.defaultTalkToMessage.Any()) {
                m = new Queue<Message>(_clickedNPC.livingData.defaultTalkToMessage);
                intro = m.ToArray() [m.Count - 1];
            } else {
                intro = new Message("Hello, my name is " + _clickedNPC.data.firstName, (LivingEntity) _clickedNPC);
                m.Enqueue(intro);
                // .SetLastMessage();

            }
            intro.options = new List<MessageOptions>();
            intro.options.Add(new MessageOptions(
                delegate() {
                    DialogueSystem.StopTalking();
                    GQuestGen.main.ReplaceNodes(_clickedNPC);
                },
                "Start a new Quest?"));
            intro.SetLastMessage();

        }
        DialogueSystem.DisplayMessageQueue(m, (LivingEntity) _clickedNPC);
        motion.GoTo(_clickedNPC.transform.position);

    }

    public void TargetLock() {
        List<Collider> hits = new List<Collider>();
        List<Entity> eHits = new List<Entity>();
        hits.AddRange(Physics.OverlapSphere(transform.position, 20));

        foreach(var collider in hits) {
            Entity e = collider.GetComponentInParent<Entity>();
            if(e != null) {
                Vector3 offset = Camera.main.transform.position - transform.position;
                float p = Vector3.Angle(Camera.main.transform.forward, e.transform.position - (Camera.main.transform.position)) / -90.0f + 1.0f;
                if(p > 0.7f) {
                    eHits.Add(e);
                }
            }
        }
        float d = Mathf.Infinity;
        eTarget = null;

        if(!eTargeting) {
            foreach(var eHit in eHits) {
                if(eHit == this || eHit == null) continue;
                float f = Vector3.Distance(transform.position, eHit.transform.position);
                if(f < d) eTarget = eHit;
            }
            if(eTarget != null) {
                Focusdirection = eTarget.transform.position - this.transform.position;
                eTargeting = true;

            } else {
                Focusdirection = Camera.main.transform.forward;
            }
        } else {
            eTargeting = false;
        }
        normalRig.m_RecenterToTargetHeading.m_enabled = eTargeting;
    }

    // public void ClickEntity(NPCEntity _clickedNPC) {

    // }

    public int curruntActionInt;

    public PlayerEntity(LivingEntityData _LivingEntityData) : base(_LivingEntityData) {}
}