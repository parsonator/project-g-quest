using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class TurnBaceBattles : MonoBehaviour {
    public static TurnBaceBattles main;
    public CinemachineTargetGroup cinemachineTargetGroup;
    public CinemachineFreeLook CCcamera;
    public ParticleSystem target;
    public List<TeamMember> AttackingParty;
    public List<TeamMember> PlayerParty;

    public AttackStage attackStage;
    // Use this for initialization

    public Vector3 BattleDirection;

    PlayerMovement mainPlayer;
    List<Transform> targs;

    [System.Serializable]
    public class TeamMember {
        public LivingEntity entity;
        [HideInInspector]
        public Vector3 poz;
        [HideInInspector]
        public Vector3 lookDirection;

    }

    public List<TeamMember> setParty(LivingEntity entity) {
        List<TeamMember> GenParty = new List<TeamMember>();
        if(entity.party.partyMembers.Count != 0) {
            foreach(LivingEntity e in entity.party.partyMembers) {
                TeamMember p = new TeamMember();
                p.entity = e;
                GenParty.Add(p);
            }
        } else {
            TeamMember p = new TeamMember();
            p.entity = entity;
            GenParty.Add(p);
            print("no Party");
        }
        return GenParty;
    }

    Vector3 battleCenter {
        get {
            return FindCentroid(targs);
        }
    }
    void Awake() {
        main = this;
    }
    void Start() {
        mainPlayer = PlayerMovement.main;
        target.gameObject.SetActive(false);
    }

    public void SetUpBattle(LivingEntity entity) {
        AttackingParty = setParty(entity);
        PlayerParty = setParty(mainPlayer);

        StartBattle();
    }

    public void StartBattle() {
        GameState.curruntState = GameState.GameModeState.COMBAT;
        cinemachineTargetGroup = GetComponentInChildren<CinemachineTargetGroup>();
        CCcamera = GetComponentInChildren<CinemachineFreeLook>();
        target.gameObject.SetActive(true);

        List<CinemachineTargetGroup.Target> camTargs = new List<CinemachineTargetGroup.Target>();
        targs = new List<Transform>();

        CinemachineTargetGroup.Target playerT = new CinemachineTargetGroup.Target();
        playerT.target = mainPlayer.transform;
        playerT.radius = 1;
        playerT.weight = 1;
        foreach(TeamMember n in AttackingParty) {
            CinemachineTargetGroup.Target npcT = new CinemachineTargetGroup.Target();
            npcT.target = n.entity.transform;
            npcT.radius = 1;
            npcT.weight = 1;
            camTargs.Add(npcT);
            targs.Add(n.entity.transform);
        }
        foreach(TeamMember n in PlayerParty) {
            CinemachineTargetGroup.Target npcT = new CinemachineTargetGroup.Target();
            npcT.target = n.entity.transform;
            npcT.radius = 1;
            npcT.weight = 1;
            camTargs.Add(npcT);
            targs.Add(n.entity.transform);
        }

        cinemachineTargetGroup.m_Targets = camTargs.ToArray();
        CCcamera.Priority = 11;

        StartCoroutine(enter());
    }

    // Update is called once per frame
    void Update() {
        switch (attackStage) {
            case AttackStage.ENTER:

                break;
            case AttackStage.ATTACK:

                break;
            case AttackStage.DEFEND:

                break;
            case AttackStage.RUN:

                break;

        }
    }

    void setUpEntitys(List<TeamMember> team, float dis) {
        Vector3 p = FindCentroid(targs);
        Vector3 player = mainPlayer.transform.position;

        Vector3 h = p - player;
        Vector3 direction = h / h.magnitude;

        Vector3 offset = Vector3.zero;
        float tick = 0;

        for(int i = 0; i < team.Count; i++) {
            float d = i * 2;

            Vector3 DirectionRight = Vector3.Cross(direction.normalized, Vector3.up.normalized);
            offset = DirectionRight * d;
            offset -= DirectionRight * team.Count / 2;
            Vector3 finalP = (player + (direction * dis)) + offset;
            team[i].poz = finalP;
            // team[i].entity.EnterCombatMode(finalP);
            team[i].entity.motion.LookAt(direction * dis);
            Debug.DrawLine(finalP, finalP + (Vector3.up * 3), Color.red, 30);

            // print((direction) + (offset));
        }
        BattleDirection = direction;

    }

    IEnumerator enter() {
        setUpEntitys(AttackingParty, 2);
        setUpEntitys(PlayerParty, -2);

        yield return new WaitForSeconds(3);
        foreach(TeamMember p in AttackingParty) {
            p.lookDirection = p.entity.transform.position - BattleDirection;
            p.entity.motion.LookAt(p.lookDirection);

        }
        foreach(TeamMember p in PlayerParty) {
            p.lookDirection = p.entity.transform.position + BattleDirection;
            p.entity.motion.LookAt(p.lookDirection);
        }
        StartCoroutine(ATTACK(AttackingParty, PlayerParty));
    }
    // float attackingDis = 1.5f;

    TeamMember findNonDead(List<TeamMember> party) {
        List<TeamMember> alive = new List<TeamMember>();
        foreach(TeamMember p in party) {
            if(!p.entity.isDead(false)) {
                alive.Add(p);
            }
        }
        if(alive.Count == 0) {
            return null;
        }
        return alive[Random.Range(0, alive.Count)];
    }

    IEnumerator AttackTeam(List<TeamMember> attacking, List<TeamMember> defending) {
        yield return null;

        // for (int i = 0; i < attacking.Count; i++)
        // {
        //     if (!attacking[i].entity.isDead(false))
        //     {
        //         target.transform.parent = attacking[i].entity.transform;
        //         target.transform.localPosition = Vector3.zero;

        //         TeamMember e = findNonDead(defending);
        //         print(attacking[i].entity.data.firstName);

        // yield return attacking[i].entity.Attack(e.entity);
        //         attacking[i].entity.motion.GoTo(attacking[i].poz);
        //         yield return new WaitUntil(() => attacking[i].entity.motion.ReachedDes(0.1f));
        //         attacking[i].entity.motion.LookAt(attacking[i].lookDirection);
        //         if (!e.entity.isDead(false)) e.entity.motion.LookAt(e.lookDirection);
        //         if (findNonDead(defending) == null)
        //         {
        //             print("Wining Team is " + attacking[i].entity.data.firstName);
        //             DeclareWinner(attacking, defending); // print("Wining Team is " + attacking[i].entity.Data.firstName);
        //         }
        //     }

        // }
    }

    public void DeclareWinner(List<TeamMember> winingTeam, List<TeamMember> loseingTeam) {
        StopAllCoroutines();
        target.gameObject.SetActive(false);
        target.transform.parent = null;

        foreach(TeamMember teamMate in winingTeam) {
            teamMate.entity.Revive();
        }

        if(winingTeam == PlayerParty) {
            GameState.curruntState = GameState.GameModeState.ADVENTURE;
            CCcamera.Priority = 5;
        } else {
            mainPlayer.Revive();
        }
    }

    IEnumerator ATTACK(List<TeamMember> attacking, List<TeamMember> defending) {
        print("ATTACK");
        attackStage = AttackStage.ATTACK;

        yield return StartCoroutine(AttackTeam(attacking, defending));

        yield return new WaitForSeconds(2);
        StartCoroutine(DEFEND(attacking, defending));
    }
    IEnumerator DEFEND(List<TeamMember> attacking, List<TeamMember> defending) {
        print("ATTACK");

        attackStage = AttackStage.DEFEND;
        yield return StartCoroutine(AttackTeam(defending, attacking));

        yield return new WaitForSeconds(2);
        StartCoroutine(ATTACK(attacking, defending));

    }
    IEnumerator RUN() {
        for(int i = 0; i < PlayerParty.Count; i++) {

            print(PlayerParty[i].entity.data.firstName);

            yield return new WaitForSeconds(2);

        }
        yield return new WaitForSeconds(2);
    }

    // IEnumerator TeamDefeated(List<Party> suviveingParty)
    // {

    // }

    private Vector3 FindCentroid(List<Transform> targets) {

        Vector3 centroid;
        Vector3 minPoint = targets[0].position;
        Vector3 maxPoint = targets[0].position;

        for(int i = 1; i < targets.Count; i++) {
            Vector3 pos = targets[i].position;
            if(pos.x < minPoint.x)
                minPoint.x = pos.x;
            if(pos.x > maxPoint.x)
                maxPoint.x = pos.x;
            if(pos.y < minPoint.y)
                minPoint.y = pos.y;
            if(pos.y > maxPoint.y)
                maxPoint.y = pos.y;
            if(pos.z < minPoint.z)
                minPoint.z = pos.z;
            if(pos.z > maxPoint.z)
                maxPoint.z = pos.z;
        }

        centroid = minPoint + 0.5f * (maxPoint - minPoint);

        return centroid;
    }

    public enum AttackStage {
        ENTER,
        ATTACK,
        DEFEND,
        DEFFEAT,
        VICTORY,
        RUN,

    }

}