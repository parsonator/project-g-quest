using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class NavMeshMotion : EntityMotion {

    public override void GoTo(Vector3 _point) {
        if(!blockMotion)
            navMeshAgent.destination = _point;
    }

    public override void ResetMotion() {}

    public override void Stop() {
        navMeshAgent.destination = transform.position;
    }

    public bool reachedDes(float distence = 0.05f) {
        float distanceToTarget = Vector3.Distance(transform.position, navMeshAgent.destination);
        return (distanceToTarget < distence);
    }
    public override void Move(Vector3 _direction) {
        if(!blockMotion)
            navMeshAgent.velocity = (_direction * speed);
    }

    public override Vector3 MovementVelocity() {
        return navMeshAgent.velocity;
    }

    // public override void LookAt(Vector3 Direction) {
    // 	transform.LookAt(Direction);
    // }

}