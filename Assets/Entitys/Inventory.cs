using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {
    public int inventoryCount = 4;
    public List<ObjectEntity> inventory = new List<ObjectEntity>();
    private Vector3 PoolSpace = new Vector3(0, -500, 0);

    public bool AddObjectToInventory(ObjectEntity _objectEntity) {
        if(inventory.Count < inventoryCount) {
            HideObject(_objectEntity);
            inventory.Add(_objectEntity);
            return true;
        }
        return false;
    }
    public ObjectEntity RemoveObjectFormInventory(ObjectEntity _objectEntity) {

        if(inventory.Contains(_objectEntity)) {
            inventory.Remove(_objectEntity);
            ShowObject(_objectEntity, transform.position + (transform.forward * 2));
            return _objectEntity;
        }
        return null;
    }
    public ObjectEntity HideObject(ObjectEntity _objectEntity) {
        _objectEntity.Freeze();
        _objectEntity.gameObject.SetActive(false);
        _objectEntity.transform.position = PoolSpace;
        return _objectEntity;
    }
    public ObjectEntity ShowObject(ObjectEntity _objectEntity, Vector3 _position) {
        _objectEntity.transform.position = _position;
        _objectEntity.Freeze(false);
        _objectEntity.gameObject.SetActive(true);
        return _objectEntity;
    }

}