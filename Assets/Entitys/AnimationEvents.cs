using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEvents : MonoBehaviour {

    public ParticleSystem attackParticleSystem;
    Transform allParticleSystems;
    void Awake() {
        if(!(allParticleSystems = transform.parent.Find("AllPS"))) {
            allParticleSystems = new GameObject("AllPS").transform;
            allParticleSystems.transform.parent = transform.parent;
            allParticleSystems.transform.localPosition = Vector3.zero;
        }
    }

    public void PlayAttack() {
        attackParticleSystem.Play();
    }

    void RemoveParticleSystems() {
        if(!allParticleSystems) return;
        for(int i = 0; i < allParticleSystems.childCount; i++) {
            Destroy(allParticleSystems.GetChild(i).gameObject);
        }
    }

    public void ReplaceAttack(GameObject prefab, Vector3 poz) {
        RemoveParticleSystems();
        Awake();
        attackParticleSystem = Instantiate(prefab, poz, prefab.transform.rotation).GetComponentInChildren<ParticleSystem>();
        attackParticleSystem.transform.parent = allParticleSystems;
        attackParticleSystem.transform.name = prefab.name;
        attackParticleSystem.transform.position = poz;
    }
}