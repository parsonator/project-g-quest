using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectTraitsOverTime : MonoBehaviour {

    public TraitsData trait;

    public float rate = 1;

    public float radius = 1;
    public Vector3 localPoz = Vector3.zero;

    float tick;

    private void OnTriggerEnter(Collider other) {

        Entity e = other.GetComponentInParent<Entity>();
        if(e) {
            if(GetComponentInParent<Entity>() == e)
                return;
            e.AddTrait(trait);
        }
    }

    private void OnTriggerExit(Collider other) {
        tick = 0;
    }
    private void OnTriggerStay(Collider other) {

        Entity e = other.GetComponentInParent<Entity>();
        if(e) {
            if(GetComponent<Entity>() == e)
                return;

            if(e != null) {
                tick += Time.deltaTime;
                if(tick > rate) {
                    e.AddTrait(trait);
                }
            }
        }
    }

    // void OnDrawGizmosSelected()
    // {
    // 	Gizmos.DrawWireSphere(transform.TransformPoint(localPoz),radius);	
    // }
}