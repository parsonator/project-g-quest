using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class QuitGame : MonoBehaviour {

    public void Quitgame() {
        Analytics.CustomEvent("Finished Demo", new Dictionary<string, object> { { "Missions Compleat", GQuestGen.missionCompleated },
            { "Quests Generatorted", GQuestGen.questsGeneratorted },
            { "Quests Compleated", GQuestGen.questsCompleated },
            { "Timed Played", Time.time },
        });

        StartCoroutine(quit());
    }

    IEnumerator quit() {
        yield return new WaitForSeconds(2);
        Application.Quit();

    }

}