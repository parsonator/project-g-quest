using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour {

    public static bool blockGameActionControles = true;

    public static bool AimDown() {
        if(blockGameActionControles) return false;
        return Input.GetButtonDown("Lock1");
    }

    public static bool Aim() {
        if(blockGameActionControles) return false;
        return Input.GetButton("Lock1");
    }
    public static bool AttackDown() {
        if(blockGameActionControles) return false;

        if(Input.GetKeyDown(KeyCode.Mouse0)) return Input.GetKeyDown(KeyCode.Mouse0);
        else if(Input.GetButtonDown("Fire1")) return Input.GetButtonDown("Fire1");
        else return false;
    }

    public static bool Attack() {
        if(blockGameActionControles) return false;
        if(Input.GetKey(KeyCode.Mouse0)) return Input.GetKey(KeyCode.Mouse0);
        else if(Input.GetButton("Fire1")) return Input.GetButton("Fire1");
        else return false;
    }
    public static bool InteractDown() {
        return Input.GetButtonDown("Use1");
    }

}