using System;
using System.Collections;
using System.Collections.Generic;
using GQuest;
using UnityEngine;

public abstract class GObjective : ScriptableObject {

    // Use this for initialization
    // public enum GQuestType{Goto}
    [NonSerialized] public int Id;
    public string GName = "";

    public string questInfo {
        get {
            return curruntObjective.objectiveName;
        }
    }
    public delegate void Event(int winNo);
    public Event onStart;
    public Event onEnd;
    public int objectiveStatusNumber;

    public GObjectiveMeta gObjectiveMeta = new GObjectiveMeta();

    public Objective curruntObjective;

    protected GObjective(int id) {

    }
    public string GetQuestInfo() {
        if(questInfo == "" || questInfo == null) return "NO_QUEST_INFO";
        return string.Format(questInfo, objectiveStatusNumber);
    }
    public abstract void UpdateObjective();
    public abstract int ObjectiveStatus();
    public virtual void OnObjectiveStart() {
        if(onStart != null)
            onStart(0);
        onStart = null;
        if(curruntObjective.onStart != null) curruntObjective.onStart();
        curruntObjective.onStart = null;

    }
    public virtual void OnObjectiveEnd(int _winNo) {
        if(onEnd != null)
            onEnd(_winNo);
        onEnd = null;
        if(curruntObjective.onEnd != null) curruntObjective.onEnd();
        curruntObjective.onEnd = null;

    }

    public static string GetNodeName(Type nodeType) {
        return nodeType.Name;
    }

    public class GObjectiveMeta {
        public RelationsRecipe BaceRelationsRecipe;
        public GRuleSet baceGRuleSet;
        public Dictionary<int, NPCEntity> NPCsUsed;
        public Dictionary<string, NPCEntity> NPCmap;
    }

}