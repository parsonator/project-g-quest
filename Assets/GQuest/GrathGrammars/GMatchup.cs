using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GQuest;
using UnityEngine;
namespace GQuest {

    public class GMatchup : ScriptableObject {

        private Dictionary<int, GNode> _mNodes = new Dictionary<int, GNode>();
        public List<GMatch> Matches = new List<GMatch>();

        public GRule Rule;
        public bool CheckSubgraphPresent(GRule rule, GGraph graph) {
            bool compareReturnValue = false;

            foreach(GNode node in graph.Nodes) {
                if(node.nodeSymbol == rule.LeftSide.Nodes.ToArray() [0].nodeSymbol) {
                    if(Compare(rule.LeftSide.Nodes.ToArray() [0], node, new List<GEdge>())) {
                        compareReturnValue = true;

                        GMatch newMatch = new GMatch();
                        newMatch.Nodes = _mNodes;
                        _mNodes = new Dictionary<int, GNode>();
                        Matches.Add(newMatch);
                    }
                    _mNodes = new Dictionary<int, GNode>();
                }
            }
            Rule = rule;
            return compareReturnValue;
        }

        private bool Compare(GNode ruleNode, GNode mainNode, List<GEdge> visitedEdges) {
            if(ruleNode.nodeSymbol != mainNode.nodeSymbol)
                return false;

            if(ruleNode.ruleId >= 0) {
                _mNodes.Add(ruleNode.ruleId, mainNode);
            }

            foreach(GEdge ruleEdge in ruleNode.outEdges) {
                if(visitedEdges.Contains(ruleEdge)) {
                    continue;
                }
                bool matched = false;
                foreach(GEdge mainEdge in mainNode.outEdges) {
                    visitedEdges.Add(ruleEdge);
                    if(Compare(ruleEdge.targetNode, mainEdge.targetNode, visitedEdges)) {
                        matched = true;
                        break;
                    }
                }
                if(matched == false)
                    return false;
            }
            return true;
        }
    }

    public class GMatch {
        public Dictionary<int, GNode> Nodes = new Dictionary<int, GNode>();
    }
}