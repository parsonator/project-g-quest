using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GQuest;
using UnityEngine;
using UnityEngine.Analytics;

public class GQuestGen : MonoBehaviour {
    public static GQuestGen main;
    public GGraph CurruntGrath;
    public List<GRuleSet> BaseProductionRules;
    public static int missionCompleated;
    public static int questsCompleated;
    public static int questsGeneratorted;
    public GNodeRef startNode;
    public GNode startMission;
    public static GNode curruntMission;
    public static GNode curruntTargetEndNode {
        get {
            return TraverseToEndNode(GQuestGen.curruntMission);
        }
    }

    public string testname;
    // Use this for initialization

    private List<GRuleSet> filteredProductionRules;

    void Awake() {
        main = this;
    }

    bool start;
    void Update() {

        if(curruntMission != null && !curruntMission.EndNode()) {
            foreach(var edgeObjective in curruntMission.outEdges) {
                if(edgeObjective.objective == null) continue;

                if(edgeObjective.startedObjective == null) {
                    edgeObjective.objective.OnObjectiveStart();
                    edgeObjective.startedObjective = true;
                }
                edgeObjective.objective.UpdateObjective();
                // print(((GObjective)curruntMission.nextNode()).questInfo);
                int winNo = edgeObjective.objective.ObjectiveStatus();
                if(winNo != -1) {
                    missionCompleated++;
                    edgeObjective.objective.OnObjectiveEnd(winNo);
                    if(!curruntMission.EndNode()) {
                        print("ding");
                        curruntMission = edgeObjective.targetNode;
                        UIManager.main.curruntMissionText(curruntMission);

                    }
                }
            }
        }
    }

    public static List<RelationsRecipe> NPCFitsRelationsRecipes(LivingEntity _livingEntity, List<RelationsRecipe> _relationsRecipes) {
        // LivingEntityData livingData = _livingEntity.livingData;

        List<RelationsRecipe> filteredRelationsRecipes = new List<RelationsRecipe>();

        foreach(NPCRelation re in _livingEntity.ActiveRelations) {
            List<RelationsRecipe> r = _relationsRecipes.FindAll(
                delegate(RelationsRecipe recipe) {
                    if(recipe == null) {
                        Debug.Log(re);
                        Debug.Log(_livingEntity.data);
                        Debug.Log(_livingEntity);
                    }
                    return (recipe.Contains(re) != null &&
                        WorldData.GetWorldEntitysData().Contains(re.trait));
                }
            );
            filteredRelationsRecipes.AddRange(r);
            // print(string.Format("{0}: from {1} there is/are {2} events", livingData.fullName, re.gRelation, filteredRelationsRecipes.Count));
        }
        return (filteredRelationsRecipes);
    }

    public static GObjective GenQuestFromNPC(NPCEntity _livingEntity, RelationsRecipe _relationsRecipe, List<NPCEntity> _whiteList) {
        GObjective g = null;
        RelationsRecipe mission = _relationsRecipe;
        Debug.LogFormat("picked mission: {0}", mission.name);
        g = _relationsRecipe.GenMissions(_livingEntity, _whiteList);
        return g;
    }

    public static void ReplaceNodesWithARule(GGraph graph, GRule rule, GMatch match) {
        List<GNode> newNodes = new List<GNode>();
        Dictionary<string, string> rrNodes = new Dictionary<string, string>();
        //remove all edges
        foreach(GNode node in match.Nodes.Values) {
            foreach(GNode node2 in match.Nodes.Values) {
                if(!Equals(node, node2)) {
                    for(int i = graph.EdgeCount - 1; i >= 0; i--) {
                        GEdge e = graph.Edges.ToArray() [i];
                        if(Equals(e.sourceNode, node) && Equals(e.targetNode, node2)) {
                            graph.RemoveEdge(e);
                        }
                    }
                }
            }
        }

        //replace nodes
        foreach(KeyValuePair<int, GNode> m in match.Nodes) {
            GNode rrNode = rule.RightSide.GetNodeWithRuleID(m.Key);
            string newSymbol;
            if(rrNode != null) {
                newSymbol = rule.RightSide.GetNodeWithRuleID(m.Key).nodeSymbol;
            } else {
                graph.RemoveNode(m.Value);
                continue;
            }

            GNode newNode = new GNode(graph.GetNewID().ToString());
            newNode.ruleId = m.Key;
            newNode.nodeSymbol = newSymbol;
            newNode.npcMap = rrNode.npcMap;
            // newNode.relationsRecipes = rrNode.relationsRecipes;

            graph.AddNode(newNode);
            rrNodes.Add(rrNode.id, newNode.id);
            newNodes.Add(newNode);

            foreach(GEdge edge in graph.Edges.ToList<GEdge>().Where(x => Equals(x.targetNode.id, m.Value.id))) {
                GEdge e = graph.AddEdge(edge.sourceNode.id, edge.sourceNode.nodeSymbol + "_" + newNode.nodeSymbol, newNode.id);
                e.baseRuleSet = edge.baseRuleSet;
                e.objectiveConstructor = edge.objectiveConstructor;
            }
            foreach(GEdge edge in graph.Edges.ToList<GEdge>().Where(x => Equals(x.sourceNode.id, m.Value.id))) {
                GEdge e = graph.AddEdge(newNode.id, newNode.nodeSymbol + "_" + edge.targetNode.nodeSymbol, edge.targetNode.id);
                e.baseRuleSet = edge.baseRuleSet;
                e.objectiveConstructor = edge.objectiveConstructor;
            }

            graph.RemoveNode(graph.Nodes.FirstOrDefault(x => x.id == m.Value.id));
        }

        foreach(GNode node in rule.RightSide.Nodes.ToList().Where(x => x.ruleId == -1)) {
            GNode newNode = new GNode(graph.GetNewID().ToString());
            newNode.ruleId = -1;
            newNode.nodeSymbol = node.nodeSymbol;
            newNode.npcMap = node.npcMap;
            // newNode.objectiveConstructor = node.objectiveConstructor;
            graph.AddNode(newNode);
            rrNodes.Add(node.id, newNode.id);
            newNodes.Add(newNode);

        }

        //copy edges
        foreach(GEdge e in rule.RightSide.Edges) {
            try {
                GNode n1 = graph.GetNode(rrNodes[e.sourceNode.id]);
                GNode n2 = graph.GetNode(rrNodes[e.targetNode.id]);
                GEdge edge = graph.AddEdge(n1.id, n1.nodeSymbol + "_" + n2.nodeSymbol, n2.id);
                edge.objectiveConstructor = e.objectiveConstructor;
                edge.baseRuleSet = e.baseRuleSet;
                // edge.npcMap = e.npcMap;
            } catch (System.Exception) {
                Debug.LogError(string.Format("ReWright fail, node {0} or {1} not real", e.sourceNode.id, e.targetNode.id));
                throw;
            }

        }

        //remove marks
        foreach(GNode newNode in newNodes) {
            newNode.ruleId = -1;
        }
    }

    public static void ResetGen() {
        WorldData.QuestUsedNPCS = new List<NPCEntity>();
        // WorldData.ReviveAll();
        curruntMission = null;
    }

    public void ReplaceNodes(NPCEntity _startNPC) {
        ResetGen();
        questsGeneratorted++;
        GGraph g = new GGraph();
        GNode n1 = startNode.GetNode(g);
        g.AddNode(n1);
        startMission = n1;

        GenReWrightRules(BaseProductionRules, g, _startNPC);

        startMission = g.Nodes.ToList().Find(x => !x.inEdges.Any()); // find node with no inEdges
        curruntMission = startMission;
        ProcessMission(startMission, _startNPC);
        startMission.Traverse(startMission, 0, true);
        CurruntGrath = g;
        UIManager.main.curruntMissionText(curruntMission);

    }

    public void ProcessMission(GNode _node, LivingEntity _startNPC) {
        List<NPCEntity> markedNPCs = WorldData.QuestUsedNPCS;
        List<NPCEntity> lastObjectiveNPCs = null;

        for(int i = 0; i < _node.outEdges.Count; i++) {

            WorldData.QuestUsedNPCS = markedNPCs;

            GEdge curruntEdge = _node.outEdges[i];

            curruntEdge.objective = curruntEdge.baseRuleSet.GenMissions(
                _startNPC, markedNPCs,
                curruntEdge);

            NPCEntity carryOnNpc = curruntEdge.objective.gObjectiveMeta.NPCsUsed.Values.ToList().Find(x => !markedNPCs.Contains(x));
            ProcessMission(_node.outEdges[i].targetNode, carryOnNpc);
        }
    }

    public List<GRuleSet> FilterRulesToNPCs(LivingEntity _livingEntity, List<GRuleSet> _gRuleSet) {
        List<GRuleSet> output = new List<GRuleSet>();
        foreach(var rule in _gRuleSet) {
            bool fits = true;
            foreach(var aRelation in rule.abstractRelationConditions) {
                if(!_livingEntity.HasRelationship(aRelation)) {
                    fits = false;
                    break;
                }
            }
            if(fits)
                output.Add(rule);
        }
        return output;
    }

    public void ReWrightRules(List<GRuleSet> _ruleSet, GGraph g) {
        int Count = 0;
        int tempgraph = 0;
        while (!Equals(tempgraph, g.NodeCount)) {
            tempgraph = g.NodeCount;
            for(int i = 0; i < _ruleSet.Count; i++) {
                GRuleSet pRule = _ruleSet[i];
                GRule r = new GRule();
                r.SetRule(_ruleSet[i].name,
                    _ruleSet[i].GenRule(pRule.leftProductionRules),
                    _ruleSet[i].GenRule(pRule.rightProductionRules));
                GMatchup mch = new GMatchup();
                if(mch.CheckSubgraphPresent(r, g)) {;
                    ReplaceMatches(mch, g);
                }
            }
            Count++;
            if(Count > 100) {
                Debug.LogError("Infinite generation loop");
                return;
            }
        }
    }

    public void GenReWrightRules(List<GRuleSet> _ruleSet, GGraph g, NPCEntity _NPCEntity) {
        int Count = 0;
        int tempgraph = 0;
        List<GRuleSet> filteredRules = FilterRulesToNPCs(_NPCEntity, _ruleSet);
        NPCEntity nextE = _NPCEntity;
        // while (!Equals(tempgraph, g.NodeCount)) {
        tempgraph = g.NodeCount;
        for(int i = 0; i < filteredRules.Count; i++) {
            GRuleSet pRule = filteredRules[i];

            GGraph lR = filteredRules[i].GenRule(pRule.leftProductionRules, nextE, new List<NPCEntity>());
            GGraph rR = filteredRules[i].GenRule(pRule.rightProductionRules, nextE, new List<NPCEntity>());

            GRule r = new GRule();
            r.SetRule(filteredRules[i].name, lR, rR);
            GMatchup mch = new GMatchup();
            if(mch.CheckSubgraphPresent(r, g)) {
                ReplaceMatches(mch, g);
            }
            // nextE = rR.FindStartNode().inEdges[0].npcMap.Values.ToArray()[0];
        }
        filteredRules = FilterRulesToNPCs(nextE, _ruleSet);
        Count++;
        if(Count > 100) {
            Debug.LogError("Infinite generation loop");
            return;
        }
        // }
    }

    void ReplaceMatches(GMatchup matchups, GGraph g) {
        foreach(GMatch item in matchups.Matches) {
            ReplaceNodesWithARule(g, matchups.Rule, item);
        }
    }
    public static GNode TraverseToEndNode(GNode _node) {
        if(!_node.outEdges.Any())
            return _node;
        for(int i = 0; i < _node.outEdges.Count; i++) {
            GNode n = TraverseToEndNode(_node.outEdges[i].targetNode);
            if(n != null) return n;
        }
        return null;
    }
}