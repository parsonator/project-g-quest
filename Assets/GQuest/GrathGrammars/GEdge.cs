using System;
using System.Collections;
using System.Collections.Generic;
using GQuest;
using UnityEngine;
namespace GQuest {
    [Serializable]
    public class GEdge {
        public GNode sourceNode;
        public GNode targetNode;
        public GObjective objective;
        public ObjectiveConstructor objectiveConstructor;
        public GRuleSet baseRuleSet;

        public bool startedObjective = false;
        // public Dictionary<string, NPCEntity> npcMap;
        public string source;
        public string target;
        public string lable;

        public GEdge(string source, string labelText, string target) {
            if(String.IsNullOrEmpty(source) || String.IsNullOrEmpty(target))
                throw new InvalidOperationException("Creating an edge with null or empty source or target IDs");
            this.source = source;
            this.target = target;

            if(!System.String.IsNullOrEmpty(labelText)) {
                lable = labelText;
            }
        }

        public GEdge(GNode _sourceNode, GNode _targetNode) {
            this.sourceNode = _sourceNode;
            this.targetNode = _targetNode;

            if(sourceNode == targetNode)
                sourceNode.AddSelfEdge(this);
            else {
                sourceNode.AddOutEdge(this);
                targetNode.AddInEdge(this);
            }
        }

    }
}