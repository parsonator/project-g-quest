using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GQuest;
using UnityEngine;
using UnityEngine.Analytics;
namespace GQuest {

    [System.Serializable]
    public class GNode {

        public string id;
        public int ruleId;
        [HideInInspector]
        public int level;
        public int orderNo;
        public string nodeSymbol;
        public Dictionary<string, NPCEntity> npcMap;

        public GNode() {

        }

        public GNode(string id) {
            this.id = id;
        }

        public void AddSelfEdge(GEdge e) {
            selfEdges.Add(e);
        }

        public List<GEdge> outEdges = new List<GEdge>();
        public List<GEdge> inEdges = new List<GEdge>();
        public List<GEdge> selfEdges = new List<GEdge>();

        public void AddInEdge(GEdge e) {
            inEdges.Add(e);
        }

        public void AddOutEdge(GEdge e) {
            outEdges.Add(e);
        }

        public void RemoveInEdge(GEdge edge) {
            inEdges.Remove(edge);
        }

        public void RemoveOutEdge(GEdge edge) {
            outEdges.Remove(edge);
        }

        public bool EndNode() {
            return (outEdges.Count == 0);
        }
        public static int branchLevel = 0;

        public void Traverse(GNode _root, int _count = 0, bool _isRoot = false) {
            if(_isRoot) {
                branchLevel = 0;
                level = 0;
                orderNo = -1;
            }

            for(int i = 0; i < _root.outEdges.Count; i++) {
                branchLevel += i;
                _root.outEdges[i].targetNode.orderNo = _count;
                _root.outEdges[i].targetNode.level = branchLevel;
                Traverse(_root.outEdges[i].targetNode, _count + 1);

            }
        }

        // public void GenTraverse(GNode _root, NPCEntity _startNPC = null) {
        //     List<NPCEntity> markedNPCs = WorldData.QuestUsedNPCS;
        //     List<NPCEntity> lastObjectiveNPCs = null;

        //     if (_startNPC != null) {
        //         Debug.Log("start Node: " + _root.nodeSymbol);

        //         objective = GQuestGen.GenQuestFromNPC(
        //             _startNPC,
        //             relationsRecipes, new List<NPCEntity>());

        //         lastObjectiveNPCs = _root.objective.gObjectiveMeta.NPCsUsed.Values.ToList();

        //     } else {
        //         lastObjectiveNPCs = _root.inEdges[0].sourceNode.objective.gObjectiveMeta.NPCsUsed.Values.ToList();
        //     }

        //     for (int i = 0; i < _root.outEdges.Count; i++) {

        //         WorldData.QuestUsedNPCS = markedNPCs;
        //         // List<NPC> lastNpcCats = lastObjectiveMeta.NPCsUsed.Values.ToList().FindAll(x => !objective.gObjectiveMeta.NPCsUsed.ContainsValue(x));
        //         Entity d = lastObjectiveNPCs[Random.Range(0, lastObjectiveNPCs.Count)];
        //         List<RelationsRecipe> rr = GQuestGen.NPCFitsRelationsRecipes(d, _root.outEdges[i].targetNode.relationsRecipes);

        //         if (!rr.Any()|| d == null) {
        //             d = WorldData.LivingWorldNPCs
        //                 .Find(x => !lastObjectiveNPCs.Contains(x)&&
        //                     GQuestGen.NPCFitsRelationsRecipes(x, _root.outEdges[i].targetNode.relationsRecipes).Any());
        //             if (d == null) {
        //                 Debug.LogErrorFormat("Error on node: {0} | last gen failed, no RelationsRecipes fit any World NPCs, Aborting ", _root.outEdges[i].targetNode.nodeSymbol);
        //                 return;
        //             } else
        //                 rr = GQuestGen.NPCFitsRelationsRecipes(d, _root.outEdges[i].targetNode.relationsRecipes);
        //         }
        //         // check if end node 
        //         if (!_root.outEdges[i].targetNode.outEdges.Any()) {
        //             _root.objective.onEnd += delegate(int _winner) {
        //                 DialogueSystem.DisplayOneMessage("Quest Compleat, Try talking to someone else", _root.objective.gObjectiveMeta.NPCsUsed[0]);
        //             };
        //             Analytics.CustomEvent("QuestCompleat", new Dictionary<string, object> { { "endnode", _root.outEdges[i].targetNode.nodeSymbol }
        //             });
        //             GQuestGen.questsCompleated++;
        //         } else {
        //             Debug.Log("Last Node: " + _root.nodeSymbol +
        //                 " | currunt Node: " + _root.outEdges[0].targetNode.nodeSymbol);
        //         }

        //         _root.outEdges[i].targetNode.objective = GQuestGen.GenQuestFromNPC(d,
        //             rr,
        //             lastObjectiveNPCs);

        //         GenTraverse(_root.outEdges[i].targetNode);
        //     }
        // }

    }

    public enum HJType {
        THE_CALL_TO_ADVENTURE,
        REFUSAL_OF_THECALL,
        SUPERNATURAL_AID,
        CROSSING_THE_FIRST_THRESHOLD,
        BELLY_OF_THE_WHALE,
        THE_ROAD_OF_TRIALS,
        MEETING_WITH_THE_GODDESS,
        TEMPTATION,
        ATONEMENT_WITH_THE_FATHER,
        APOSTASIS,
        THE_ULTIMATEBOON,
        MAGIC_FLIGHT,
        RESCUEFROM_WITHOUT,
        RETURN,
        MASTER_OF_TWO_WORLDS,
        FREEDOM_TO_LIVE
    }
}