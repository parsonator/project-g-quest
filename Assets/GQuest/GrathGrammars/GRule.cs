using System.Collections;
using System.Collections.Generic;
using GQuest;
using UnityEngine;
namespace GQuest {
    [CreateAssetMenu(fileName = "GRule", menuName = "GQuest/GRule", order = 1)]
    public class GRule : ScriptableObject {

        public string Name;
        [Space(1)]
        [Header("RightSide")]
        public GGraph RightSide;
        [Space(1)]
        [Header("LeftSide")]
        public GGraph LeftSide;

        public void SetRule(string name, GGraph leftSide, GGraph rightSide) {
            Name = name;
            LeftSide = leftSide;
            RightSide = rightSide;
        }
    }
}