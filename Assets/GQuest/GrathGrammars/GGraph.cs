using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GQuest;
using UnityEngine;
namespace GQuest {
    [Serializable]
    public class GGraph {

        internal Hashtable nodeMap = new Hashtable();
        // public List<GNode> Nodes = new List<GNode>();
        // public List<GEdge> Edges = new List<GEdge>();

        public GNode FindStartNode() {
            return Nodes.ToList().Find(x => !x.inEdges.Any());
        }
        public GNode FindEndNodes() {
            return Nodes.ToList().Find(x => !x.outEdges.Any());
        }
        public IEnumerable<GNode> Nodes {
            get {
                foreach(var r in nodeMap.Values)
                    yield return (GNode) r;
            }
        }
        public IEnumerable<GEdge> Edges {
            get {
                foreach(GNode node in Nodes) {
                    foreach(GEdge e in node.outEdges)
                        yield return e;
                }
            }
        }

        public int NodeCount {
            get { return nodeMap.Count; }
        }

        public int EdgeCount {
            get { return Edges.Count(); }
        }

        public virtual GEdge AddEdge(string source, string edgeLabel, string target) {
            string l = edgeLabel;
            if(l == null)
                l = "";
            GEdge edge = new GEdge(source, l, target);
            edge.sourceNode = AddNode(source);
            edge.targetNode = AddNode(target);

            AddPrecalculatedEdge(edge);
            return edge;
        }

        public virtual GEdge AddEdge(GNode source, string edgeLabel, GNode target) {
            return AddEdge(source.id, edgeLabel, target.id);
        }

        public virtual GEdge AddEdge(GNode source, GNode target) {
            return AddEdge(source.id, null, target.id);
        }
        public virtual GEdge AddEdge(string source, string target) {
            return AddEdge(source, null, target);
        }

        public void AddPrecalculatedEdge(GEdge edge) {
            if(edge.source != edge.target) {
                edge.sourceNode.AddOutEdge(edge);
                edge.targetNode.AddInEdge(edge);
            } else
                edge.sourceNode.AddSelfEdge(edge);
        }

        public void AddNode(GNode node) {
            if(nodeMap.ContainsKey(node.id))
                return;
            nodeMap[node.id] = node;
        }

        public GNode AddNode(string node) {
            GNode newNode = new GNode(node);
            if(nodeMap.ContainsKey(newNode.id)) {
                newNode = (GNode) nodeMap[newNode.id];
                return newNode;
            }
            nodeMap[newNode.id] = newNode;
            return newNode;
        }

        public GNode GetNodeWithRuleID(int id) {
            return Nodes.ToList().Exists(x => x.ruleId == id) ?
                Nodes.ToList().FirstOrDefault(x => x.ruleId == id) :
                null;
        }

        public virtual void RemoveEdge(GEdge edge) {
            if(edge == null)
                return;
            GNode source = edge.sourceNode;
            GNode target = edge.targetNode;
            if(source != target) {
                source.RemoveOutEdge(edge);
                target.RemoveInEdge(edge);
            }
        }

        /// <summary>
        /// Removes a node and all of its edges. If the node doesn't exist, nothing happens.
        /// </summary>
        /// <param name="node">node reference</param>
        public virtual void RemoveNode(GNode node) {
            if(node == null || !nodeMap.ContainsKey(node.id))
                return;
            var delendi = new ArrayList();
            foreach(GEdge e in node.inEdges)
                delendi.Add(e);
            foreach(GEdge e in node.outEdges)
                delendi.Add(e);
            foreach(GEdge e in delendi)
                RemoveEdge(e);
            nodeMap.Remove(node.id);
        }

        public GNode GetNode(string ID) {
            return Nodes.ToList().FirstOrDefault(x => x.id == ID);
        }

        public int IDs = 0;
        public int GetNewID() {
            IDs++;
            return IDs;
        }

    }
}