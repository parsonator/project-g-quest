using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillObjective : ObjectiveTemplate {

    bool done;

    public KillObjective(Entity _subject) : base(_subject) {}

    public override bool Objective() {
        return done;

    }
    public override void OnStart() {
        DialogueSystem.onDialogueCompletion += delegate() { done = DialogueSystem.main.curruntTarget == subject; };
    }
    public override void OnEnd() {
        DialogueSystem.onDialogueCompletion = null;
    }

}