using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ObjectiveTemplate {
    protected readonly Entity subject;
    public ObjectiveTemplate(Entity _subject) {
        subject = _subject;
    }

    public abstract bool Objective();
    public abstract void OnStart();
    public abstract void OnEnd();

}