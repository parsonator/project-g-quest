using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GGenericQuest", menuName = "GQuest/GGenericQuest", order = 1)]

[System.Serializable]
public class GGenericQuest : GObjective {

    // public GGenericQuest(int id, LivingEntity _targ, Objective.GoalDel _goal): base(id) {

    //     Objective NewObjective = new Objective(_targ, _goal);
    //     curruntObjective = new List<Objective>();
    //     curruntObjective.Add(NewObjective);

    // }
    // public GGenericQuest(int id, Objective.GoalDel _goal): base(id) {

    //     Objective NewObjective = new Objective(_goal);
    //     curruntObjective = new List<Objective>();
    //     curruntObjective.Add(NewObjective);

    // }
    // public GGenericQuest(int id, Objective[] objectives): base(id) {
    //     curruntObjective = new List<Objective>();
    //     curruntObjective.AddRange(objectives);
    // }
    public GGenericQuest(int id) : base(id) {

    }
    public override void UpdateObjective() {
        if(curruntObjective.goal == null) return;
        bool done = curruntObjective.goal();
        if(done && !curruntObjective.complete) {
            curruntObjective.complete = true;
            objectiveStatusNumber++;
        }
    }

    public override int ObjectiveStatus() {
        if(curruntObjective.complete) return 0;
        else
            return -1;
    }

}

[System.Serializable]
public class Objective {
    public string objectiveName;
    public LivingEntity target;
    public delegate void Event();
    public delegate bool GoalDel();
    public Event onStart;
    public Event onEnd;
    public GoalDel goal;
    public bool complete = false;

    public Objective(LivingEntity _targ, GoalDel _goal) {
        this.target = _targ;
        this.goal += _goal;
    }

    public Objective(GoalDel _goal) {
        this.target = null;
        this.goal += _goal;
    }

    public Objective() {}
}