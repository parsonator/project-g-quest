using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GQuest;
using UnityEngine;

namespace GQuest {

    [CreateAssetMenu(fileName = "GRuleSet", menuName = "GQuest/GRuleSet", order = 1)]
    public class GRuleSet : ScriptableObject {
        [HideInInspector]
        public List<AbstractRelation> abstractRelationConditions;

        [Header("Left Side Rule")]
        public GrathRlue leftProductionRules;
        [Header("Right Side Rule")]
        public GrathRlue rightProductionRules;

        public int OnStartMessageQueueNum = 0;
        [HideInInspector]
        public int OnEndMessageQueueNum = 0;
        [HideInInspector]

        public List<MissionMessage> OnStartMessageQueue = new List<MissionMessage>();
        [HideInInspector]
        public List<MissionMessage> OnEndMessageQueue = new List<MissionMessage>();

        public GGraph GenRule(GrathRlue ruleset) {
            GGraph g = new GGraph();

            foreach(var n in ruleset.GrathNodes) {
                GNode node = new GNode(g.GetNewID().ToString());
                node.nodeSymbol = n.name;
                node.ruleId = n.ruleID;
                // node.relationsRecipes = n.relationsRecipes;
                g.AddNode(node);
            }

            foreach(var e in ruleset.GrathEdges) {
                GEdge edge = g.AddEdge(e.source, null, e.target);
                edge.objectiveConstructor = e.objectiveConstructor;
                // edge.npcMap = ;
                edge.baseRuleSet = this;
            }

            return g;
        }
        public GGraph GenRule(GrathRlue ruleset, NPCEntity _npc, List<NPCEntity> _whiteList) {
            GGraph g = new GGraph();

            foreach(var n in ruleset.GrathNodes) {
                GNode node = new GNode(g.GetNewID().ToString());
                node.nodeSymbol = n.name;
                node.ruleId = n.ruleID;

                if(g.FindStartNode() == null)
                    node.npcMap = WorldData.NPCDataFitsProduction(_npc, _whiteList, abstractRelationConditions);
                else {
                    node.npcMap = g.FindStartNode().npcMap;

                    Dictionary<string, NPCEntity> nm = WorldData.NPCDataFitsProduction(_npc, _whiteList, abstractRelationConditions);
                    foreach(var npc in nm) {
                        if(!node.npcMap.ContainsKey(npc.Key)) {
                            node.npcMap.Add(npc.Key, npc.Value);
                        }
                    }
                }
                // node.relationsRecipes = n.relationsRecipes;
                g.AddNode(node);
            }

            foreach(var e in ruleset.GrathEdges) {
                GEdge edge = g.AddEdge(e.source, null, e.target);
                edge.objectiveConstructor = e.objectiveConstructor;
                edge.baseRuleSet = this;
            }

            return g;
        }

        public GObjective GenMissions(LivingEntity _entity, List<NPCEntity> _whiteList, GEdge _edge) {

            ObjectiveConstructor.GoalObjective gq = _edge.objectiveConstructor.goalObjective;
            // Dictionary<string, NPCEntity> map = WorldData.NPCDataFitsProduction(_entity, _whiteList, abstractRelationConditions);
            Dictionary<string, NPCEntity> map = _edge.sourceNode.npcMap;
            Dictionary<int, NPCEntity> usedNPCs = new Dictionary<int, NPCEntity>();
            int id = 0;
            if(map != null) {
                usedNPCs.Add(id++, _entity as NPCEntity);

                string questInfo = "", GName = "";
                Objective objective = new Objective();
                List<GObjective> outputGObjective = new List<GObjective>();

                GObjective n = new GGenericQuest((int) Time.time);

                if(gq.type == ObjectiveConstructor.MissionType.KILL) {
                    if(gq.target != null) {

                        NPCEntity subject = WorldData.FindNPCByData((map[gq.target]).data, _whiteList);
                        Objective o = new Objective(subject,
                            delegate() {
                                return (subject.isDead(false));
                            }
                        );
                        o.onStart += delegate() {
                            if(gq.MessageQueue.Any()) {
                                DialogueSystem.MissionMessages.Add(subject, GetMissionMessage(gq.MessageQueue, _entity, map));
                            }
                            // DialogueSystem.onDialogueCompletion += delegate() {
                            //     if (subject == DialogueSystem.main.curruntTarget)
                            //         subject.AttackEntity(PlayerEntity.main);
                            // };
                        };

                        o.onEnd += delegate() {
                            DialogueSystem.onDialogueCompletion = null;
                        };

                        usedNPCs.Add(id++, subject);
                        WorldData.QuestUsedNPCS.Add(subject);
                        o.objectiveName = "Kill " + subject.data.fullName;

                        objective = o;
                        // o.objectiveName ;
                    }
                } else if(gq.type == ObjectiveConstructor.MissionType.TALK) { //TalkTo Quest
                    if(gq.target != null) {
                        bool done = false;
                        NPCEntity subject = WorldData.FindNPCByData((map[gq.target]).data, usedNPCs.Values.ToList());
                        Objective o = new Objective(subject,
                            delegate() {
                                return (done);
                            }
                        );

                        o.onStart += delegate() {
                            if(gq.MessageQueue.Any()) {
                                DialogueSystem.MissionMessages.Add(subject, GetMissionMessage(gq.MessageQueue, _entity, map));
                            }

                            DialogueSystem.onDialogueCompletion += delegate() { done = DialogueSystem.main.curruntTarget == subject; };
                        };

                        o.onEnd += delegate() {
                            DialogueSystem.onDialogueCompletion = null;
                        };

                        // usedNPCs.Add(id++, subject);
                        o.objectiveName = "Talk to " + subject.data.fullName;

                        objective = o;
                    }
                } else if(gq.type == ObjectiveConstructor.MissionType.ATTACK_PLAYER_EVENT) { //TalkTo Quest
                    if(gq.target != null) {
                        bool done = false;
                        NPCEntity subject = WorldData.FindNPCByData((map[gq.target]).data, usedNPCs.Values.ToList());
                        Objective o = new Objective(subject,
                            delegate() {
                                return (subject.isDead(false));
                            }
                        );
                        o.onStart += delegate() {
                            if(gq.MessageQueue.Any()) {
                                DialogueSystem.MissionMessages.Add(subject, GetMissionMessage(gq.MessageQueue, _entity, map));
                            }
                            subject.AttackEntity(PlayerEntity.main);
                            // DialogueSystem.onDialogueCompletion += delegate() { done = DialogueSystem.main.curruntTarget == subject; };
                        };

                        o.onEnd += delegate() {
                            DialogueSystem.onDialogueCompletion = null;
                        };
                        WorldData.QuestUsedNPCS.Add(subject);

                        // usedNPCs.Add(id++, subject);
                        o.objectiveName = subject.data.fullName + " is attacking you!";

                        objective = o;
                    }
                }

                GName = this.name;

                n.onStart += delegate(int winNo) {
                    if(OnStartMessageQueue.Any())
                        DialogueSystem.DisplayMessageQueue(GetMissionMessage(OnStartMessageQueue, _entity, map), _entity);

                };

                n.onEnd += delegate(int winNo) {
                    if(OnEndMessageQueue.Any())
                        DialogueSystem.DisplayMessageQueue(GetMissionMessage(OnEndMessageQueue, _entity, map), _entity);
                };
                n.curruntObjective = objective;
                n.GName = GName;
                n.gObjectiveMeta.NPCsUsed = usedNPCs;
                // n.gObjectiveMeta.baceGRuleSet = this;
                n.gObjectiveMeta.NPCmap = map;

                return n;
            }
            return null;
        }

        public Queue<Message> GetMissionMessage(List<MissionMessage> _messageQueue, LivingEntity _giver, Dictionary<string, NPCEntity> _map) {
            Queue<Message> output = new Queue<Message>();
            foreach(var missionMessage in _messageQueue) {
                Message message = new Message(missionMessage.message);
                if(_map.ContainsKey(missionMessage.dataName))
                    message.target = _map[missionMessage.dataName];
                else message.target = _giver;

                output.Enqueue(message);
            }
            return output;

        }

        [System.Serializable]
        public class GrathRlue {
            public List<GrathNode> GrathNodes;
            public List<GrathEdge> GrathEdges;

        }

        [System.Serializable]

        public class GrathNode {
            public string name;
            public int ruleID;
        }

        [System.Serializable]

        public class GrathEdge {
            public string source;
            public string target;
            public ObjectiveConstructor objectiveConstructor;
        }
    }
}

[System.Serializable]
public class ObjectiveConstructor {
    public int goalObjectiveNum = 0;
    public GoalObjective goalObjective = new GoalObjective();
    [System.Serializable]
    public class GoalObjective {
        [HideInInspector]
        public int targetNo = 0;
        public string target;
        public ObjectiveTemplate objectiveTemplate;
        public MissionType type;
        public int MessageQueueNum = 0;
        // [SerializeField]
        public int displayMessageAt = 0;
        public List<MissionMessage> MessageQueue = new List<MissionMessage>();
    }
    public enum MissionType {
        KILL,
        TALK,
        ATTACK_PLAYER_EVENT,
        TAKE,
        GOTO,
        NA
    }
}