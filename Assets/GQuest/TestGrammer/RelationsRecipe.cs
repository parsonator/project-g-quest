using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GQuest;
using UnityEngine;
namespace GQuest {

    [CreateAssetMenu(fileName = "GRelationsRecipes", menuName = "GQuest/RelationsRecipes", order = 1)]
    public class RelationsRecipe : ScriptableObject {
        [HideInInspector]
        public HJType herosJourneyType;
        [HideInInspector]
        public List<AbstractRelation> abstractRelationConditions;
        [HideInInspector]
        public int OnStartMessageQueueNum = 0;
        [HideInInspector]
        public int OnEndMessageQueueNum = 0;
        [HideInInspector]
        public List<MissionMessage> OnStartMessageQueue = new List<MissionMessage>();
        [HideInInspector]
        public List<MissionMessage> OnEndMessageQueue = new List<MissionMessage>();
        [HideInInspector]
        public GQGoal Quest;
        public Data Contains(NPCRelation _NPCRelation) {
            // Debug.Log(JsonUtility.ToJson(new GQGoal()));
            foreach(AbstractRelation aa in abstractRelationConditions) {
                if(aa.relation == _NPCRelation.CurruntRelation()) {
                    if(_NPCRelation.trait.GetType().ToString() ==
                        aa.secondPartyDataType.ToString()) {
                        return (Data) _NPCRelation.trait;
                    } else if(aa.secondPartyDataType == DataType.Specify) {
                        if(_NPCRelation.trait.HasData(aa.secondPartySpecifiedData))
                            return (Data) _NPCRelation.trait;
                    } else if(aa.secondPartyDataType == DataType.Data) {
                        return (Data) _NPCRelation.trait;
                    }

                }
            }
            if(!abstractRelationConditions.Any()) {
                return (Data) _NPCRelation.trait;
            }
            return null;
        }

        // public Dictionary<string, NPCEntity> NPCDataFitsProduction(LivingEntity _entity, List<NPCEntity> _whiteList) {

        //     // LivingEntityData entityData = _entity.livingData;
        //     Dictionary<string, NPCEntity> output = new Dictionary<string, NPCEntity>();
        //     List<NPCEntity> whiteList = new List<NPCEntity>();
        //     List<NPCRelation> usedNPCRelation = new List<NPCRelation>();

        //     foreach (AbstractRelation item in abstractRelationConditions) {
        //         whiteList.AddRange(output.Values);
        //         if (!output.ContainsKey(item.dataName)) {
        //             NPCEntity outputEntity = null;
        //             switch (item.dataType) {
        //                 case DataType.TraitsData:
        //                     NPCRelation traitsDataRelation = _entity.ActiveRelations.Find(
        //                         delegate(NPCRelation r) {
        //                             return (r.CurruntRelation() == item.relation &&
        //                                 r.trait.GetType().ToString()== DataType.TraitsData.ToString()&&
        //                                 !usedNPCRelation.Contains(r));
        //                         });

        //                     TraitsData traitsData = null;
        //                     if (traitsDataRelation != null) {
        //                         usedNPCRelation.Add(traitsDataRelation);

        //                         traitsData = (TraitsData)traitsDataRelation.trait;
        //                         NPCEntity nPC = WorldData.FindNPCByData(traitsData, whiteList);
        //                         if (nPC == null)
        //                             Debug.LogError(traitsData.name + " is not in world ");
        //                         outputEntity = nPC;
        //                     } else {
        //                         Debug.LogError("nothing for " + _entity.data);
        //                     }
        //                     break;

        //                 case DataType.NPCData:
        //                     Debug.Log(item.dataName);
        //                     NPCRelation npcDataRelation = _entity.ActiveRelations.Find(
        //                         delegate(NPCRelation r) {
        //                             return (r.CurruntRelation() == item.relation &&
        //                                 r.trait.GetType().ToString()== DataType.NPCData.ToString()&&
        //                                 !usedNPCRelation.Contains(r));
        //                         });

        //                     LivingEntityData npcdata = null;
        //                     if (npcDataRelation != null) {
        //                         usedNPCRelation.Add(npcDataRelation);

        //                         npcdata = (LivingEntityData)npcDataRelation.trait;
        //                         NPCEntity nPC = WorldData.FindNPCByData(npcdata, whiteList);
        //                         if (nPC == null)
        //                             Debug.LogError(npcdata.name + " is not in world ");

        //                         outputEntity = nPC;
        //                     } else {
        //                         Debug.LogError("nothing for " + _entity.data);
        //                     }
        //                     break;

        //                 case DataType.RaceData:
        //                     NPCRelation raceDataRelation = _entity.ActiveRelations.Find(
        //                         delegate(NPCRelation r) {
        //                             return (r.CurruntRelation() == item.relation &&
        //                                 r.trait.GetType().ToString()== DataType.RaceData.ToString()&&
        //                                 !usedNPCRelation.Contains(r));
        //                         });
        //                     RaceData raceData = null;
        //                     if (raceDataRelation != null) {
        //                         usedNPCRelation.Add(raceDataRelation);

        //                         raceData = (RaceData)raceDataRelation.trait;
        //                         NPCEntity nPC = WorldData.FindNPCByData(raceData, whiteList);
        //                         if (nPC == null)
        //                             Debug.LogError(raceData.name + " is not in world ");
        //                         outputEntity = nPC;
        //                     } else {
        //                         Debug.LogError("nothing for " + _entity.data);
        //                     }
        //                     break;
        //                 case DataType.Data:
        //                     NPCRelation DataRelation = _entity.ActiveRelations.Find(
        //                         delegate(NPCRelation r) {
        //                             return (r.CurruntRelation() == item.relation &&
        //                                 !usedNPCRelation.Contains(r));
        //                         });
        //                     Data data = null;
        //                     if (DataRelation != null) {
        //                         usedNPCRelation.Add(DataRelation);

        //                         data = (Data)DataRelation.trait;
        //                         NPCEntity nPC = WorldData.FindNPCByData(data, whiteList);
        //                         if (nPC == null)
        //                             Debug.LogError(data.name + " is not in world ");
        //                         outputEntity = nPC;
        //                     } else {
        //                         Debug.LogError("nothing for " + _entity.data);
        //                     }
        //                     break;
        //                 case DataType.Specify:
        //                     NPCRelation SpecifiedRelation = _entity.ActiveRelations.Find(
        //                         delegate(NPCRelation r) {
        //                             return (r.CurruntRelation() == item.relation &&
        //                                 r.trait.HasData(item.SpecifiedData)&&
        //                                 !usedNPCRelation.Contains(r));
        //                         });
        //                     Data Specifieddata = null;
        //                     if (SpecifiedRelation != null) {
        //                         usedNPCRelation.Add(SpecifiedRelation);

        //                         Specifieddata = (Data)SpecifiedRelation.trait;
        //                         NPCEntity nPC = WorldData.FindNPCByData(Specifieddata, whiteList);
        //                         if (nPC == null)
        //                             Debug.LogError(Specifieddata.name + " is not in world ");
        //                         outputEntity = nPC;
        //                     } else {
        //                         Debug.LogError("nothing for " + _entity.data);
        //                     }
        //                     break;
        //             }
        //             if (outputEntity != null) {
        //                 if (item.dataType == DataType.Specify)
        //                     output.Add(item.SpecifiedData.name, outputEntity);
        //                 else
        //                     output.Add(item.dataName, outputEntity);
        //             } else {
        //                 if (item.dataType == DataType.Specify)
        //                     output.Add(item.SpecifiedData.name, WorldData.RandomLivingWorldNPCWithData(item.SpecifiedData, whiteList));
        //                 else
        //                     output.Add(item.dataName, WorldData.RandomLivingWorldNPC(whiteList));
        //             }
        //         }
        //     }
        //     return output;
        // }

        public GObjective GenMissions(NPCEntity _entity, List<NPCEntity> _whiteList) {

            GQGoal gq = Quest;
            Dictionary<string, NPCEntity> map = WorldData.NPCDataFitsProduction(_entity, _whiteList, abstractRelationConditions);
            Dictionary<int, NPCEntity> usedNPCs = new Dictionary<int, NPCEntity>();
            int id = 0;
            if(map != null) {
                usedNPCs.Add(id++, _entity as NPCEntity);

                string questInfo = "", GName = "";
                Objective objective = new Objective();

                GObjective n = new GGenericQuest((int) Time.time);

                if(gq.goalObjective != null) {
                    foreach(var g in gq.goalObjective) {
                        if(g.type == GQGoal.MissionType.KILL) {
                            if(g.target != null) {

                                NPCEntity subject = WorldData.FindNPCByData((map[g.target]).data, _whiteList);
                                Objective o = new Objective(subject,
                                    delegate() {
                                        return (subject.isDead(false));
                                    }
                                );
                                o.onStart += delegate() {
                                    if(g.MessageQueue.Any()) {
                                        DialogueSystem.MissionMessages.Add(subject, GetMissionMessage(g.MessageQueue, _entity, map));
                                    }
                                    DialogueSystem.onDialogueCompletion += delegate() {
                                        if(subject == DialogueSystem.main.curruntTarget)
                                            TurnBaceBattles.main.SetUpBattle(subject);
                                    };
                                };

                                o.onEnd += delegate() {

                                    DialogueSystem.onDialogueCompletion = null;
                                };

                                usedNPCs.Add(id++, subject);
                                WorldData.QuestUsedNPCS.Add(subject);
                                o.objectiveName = "Kill " + subject.data.fullName;

                                objective = o;
                                // o.objectiveName ;
                            }
                        }
                        if(g.type == GQGoal.MissionType.TALK) { //TalkTo Quest
                            if(g.target != null) {
                                bool done = false;
                                NPCEntity subject = WorldData.FindNPCByData((map[g.target]).data, usedNPCs.Values.ToList());
                                Objective o = new Objective(subject,
                                    delegate() {
                                        return (done);
                                    }
                                );

                                o.onStart += delegate() {
                                    if(g.MessageQueue.Any()) {
                                        DialogueSystem.MissionMessages.Add(subject, GetMissionMessage(g.MessageQueue, _entity, map));
                                    }
                                    DialogueSystem.onDialogueCompletion += delegate() { done = DialogueSystem.main.curruntTarget == subject; };
                                };

                                o.onEnd += delegate() {
                                    DialogueSystem.onDialogueCompletion = null;
                                };

                                usedNPCs.Add(id++, subject);
                                o.objectiveName = "talk to " + subject.data.fullName;

                                objective = o;
                            }
                        }
                    }
                } else {
                    Objective o = new Objective(_entity,
                        delegate() {
                            return (true);
                        }
                    );

                }
                GName = this.name;

                n.onStart += delegate(int winNo) {
                    if(OnStartMessageQueue.Any())
                        DialogueSystem.DisplayMessageQueue(GetMissionMessage(OnStartMessageQueue, _entity, map), _entity);

                };

                n.onEnd += delegate(int winNo) {
                    if(OnEndMessageQueue.Any())
                        DialogueSystem.DisplayMessageQueue(GetMissionMessage(OnEndMessageQueue, _entity, map), _entity);
                };
                n.curruntObjective = objective;
                n.GName = GName;
                n.gObjectiveMeta.NPCsUsed = usedNPCs;
                n.gObjectiveMeta.BaceRelationsRecipe = this;
                n.gObjectiveMeta.NPCmap = map;

                return n;
            }
            return null;
        }

        public Queue<Message> GetMissionMessage(List<MissionMessage> _messageQueue, LivingEntity _giver, Dictionary<string, NPCEntity> _map) {
            Queue<Message> output = new Queue<Message>();
            foreach(var missionMessage in _messageQueue) {
                Message message = new Message(missionMessage.message);
                if(_map.ContainsKey(missionMessage.dataName))
                    message.target = _map[missionMessage.dataName];
                else message.target = _giver;

                output.Enqueue(message);
            }
            return output;

        }

    }

    [System.Serializable]
    public class GQGoal {

        public int goalObjectiveNum = 0;

        public List<GoalObjective> goalObjective = new List<GoalObjective>();

        [System.Serializable]
        public class GoalObjective {

            [HideInInspector]
            public int targetNo = 0;
            public string target;
            public MissionType type;
            public int MessageQueueNum = 0;

            public List<MissionMessage> MessageQueue = new List<MissionMessage>();

        }

        public enum MissionType {
            KILL,
            TALK
        }

    }

    [System.Serializable]
    public class MissionMessage {
        // [TextArea(2, 2)]

        public string message;
        public int dataNameNo = 0;

        public string dataName;

        public static Queue<Message> GetMissionMessage(List<MissionMessage> _messageQueue, LivingEntity _giver, Dictionary<string, NPCEntity> _map) {
            Queue<Message> output = new Queue<Message>();
            foreach(var missionMessage in _messageQueue) {
                Message message = new Message(missionMessage.message);
                if(_map.ContainsKey(missionMessage.dataName))
                    message.target = _map[missionMessage.dataName];
                else message.target = _giver;

                output.Enqueue(message);
            }
            return output;

        }

    }

}