﻿using System.Collections;
using System.Collections.Generic;
using GQuest;
using UnityEngine;
[CreateAssetMenu(fileName = "GNodeRef", menuName = "GQuest/GNodeRef", order = 1)]

public class GNodeRef : ScriptableObject {
	[SerializeField] string nodeName;
	public GNode GetNode(GGraph g) {
		GNode n = new GNode(g.GetNewID().ToString());
		n.nodeSymbol = nodeName;
		return n;

	}
}