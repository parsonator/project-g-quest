using System.Collections;
using System.Collections.Generic;
using GQuest;
using UnityEngine;

[System.Serializable]
public class Rule {
    public GNode nonTerminal;
    public GNode replace;

    // public float probability = 1.0f;
}