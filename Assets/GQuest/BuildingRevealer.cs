using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingRevealer : MonoBehaviour {

    Transform inside, outside;
    Transform fDoor;
    public bool inZone;

    // Use this for initialization
    void Start() {
        inside = transform.Find("Inside");
        outside = transform.Find("Outside");
        // fDoor = GameObject.Find("/Door").transform;

    }

    public Transform getBildingDoor() {
        return fDoor;
    }

    public void enterBuilding() {
        inside.gameObject.SetActive(inZone);
        outside.gameObject.SetActive(!inZone);
    }

    // Update is called once per frame
    void Update() {
        enterBuilding();
    }

    void OnTriggerStay(Collider other) {
        if(PlayerMovement.main != null) {
            if(other.transform.tag == PlayerMovement.main.transform.tag)
                inZone = true;
        }
    }
    void OnTriggerExit(Collider other) {
        if(other.gameObject.tag == PlayerMovement.main.gameObject.tag)
            inZone = false;
    }

}