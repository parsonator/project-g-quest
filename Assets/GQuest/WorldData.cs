using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WorldData : MonoBehaviour {
    public static WorldData main;

    // public static List<NPCEntity> LivingWorldNPCs {
    //     get {
    //         List<NPCEntity> a = GetWorldNPCs().FindAll(
    //             delegate(NPCEntity npc) {
    //                 return (!npc.isDead()&& !QuestUsedNPCS.Contains(npc)&& npc.livingData.ExudeFromGenoration == false);
    //             });
    //         return a;
    //     }
    // }
    public static List<NPCEntity> QuestUsedNPCS = new List<NPCEntity>();

    public static List<LivingEntity> GetWorldLivingEntitys() {
        List<LivingEntity> l = new List<LivingEntity>();
        LivingEntity[] entity = GameObject.FindObjectsOfType(typeof(LivingEntity)) as LivingEntity[];
        l.AddRange(entity);
        return l;
    }
    public static List<LivingEntity> GetWorldLivingEntitysEditor() {
        List<LivingEntity> l = new List<LivingEntity>();
        LivingEntity[] entity = Resources.FindObjectsOfTypeAll(typeof(LivingEntity)) as LivingEntity[];
        l.AddRange(entity);
        return l;
    }
    public static List<NPCEntity> GetLivingWorldNPCs() {
        List<NPCEntity> a = GetWorldNPCs().FindAll(
            npc =>(!npc.isDead() && !QuestUsedNPCS.Contains(npc)));
        if(!a.Any()) return new List<NPCEntity>();
        return a;
    }

    public static List<Data> GetWorldEntitysData() {
        List<LivingEntity> we = GetWorldLivingEntitys();
        List<Data> data = new List<Data>();

        foreach(var ent in we)
            data.Add(ent.data);

        return data;
    }

    public static void ReviveAll() {
        foreach(var ent in GetWorldLivingEntitys())
            ent.Revive();
    }

    public static List<LivingEntityData> GetWorldLivingEntitysData() {
        List<NPCEntity> we = GetLivingWorldNPCs();
        List<LivingEntityData> data = new List<LivingEntityData>();

        foreach(var ent in we)
            data.Add(ent.livingData);
        return data;
    }

    public static List<NPCEntity> GetWorldNPCs() {
        List<NPCEntity> l = new List<NPCEntity>();
        NPCEntity[] npc = Resources.FindObjectsOfTypeAll(typeof(NPCEntity)) as NPCEntity[];
        l.AddRange(npc);
        return l;

    }

    public static NPCEntity RandomLivingWorldNPC() {
        List<NPCEntity> npcs = GetLivingWorldNPCs();
        if(npcs.Count == 0) return null;
        NPCEntity n = npcs[Random.Range(0, npcs.Count)];
        // Debug.Log(n.Data.fullName);
        return n;
    }
    public static NPCEntity RandomWorldNPC() {
        List<NPCEntity> npcs = GetWorldNPCs();
        NPCEntity n = npcs[Random.Range(0, npcs.Count)];
        // Debug.Log(n.Data.fullName);
        return n;
    }
    public static NPCEntity RandomLivingWorldNPC(List<NPCEntity> _whiteList) {
        List<NPCEntity> npcs = GetLivingWorldNPCs().FindAll(x => !_whiteList.Contains(x));

        NPCEntity n = npcs[Random.Range(0, npcs.Count)];

        // Debug.Log(n.Data.fullName);
        return n;
    }
    public static NPCEntity RandomLivingWorldNPCWithData(Data _data, List<NPCEntity> _whiteList) {
        List<NPCEntity> npcs = GetLivingWorldNPCs().FindAll(x => !_whiteList.Contains(x) && x.data.HasData(_data));
        NPCEntity n = npcs[Random.Range(0, npcs.Count)];
        return n;
    }
    public static NPCEntity FindNPCByData(Data _data) {
        NPCEntity candidate = GetLivingWorldNPCs().Find(
            delegate(NPCEntity npc) {
                return (npc.data.HasData(_data));
            }
        );
        if(candidate != null)
            return candidate;

        Debug.LogError("No NPC Fits, Returning Random");
        return RandomLivingWorldNPC();

    }

    public static NPCEntity FindNPCByData(Data _data, List<NPCEntity> _whiteList) {

        NPCEntity candidate = GetLivingWorldNPCs().Find(x => x.HasData(_data));
        // NPCEntity candidate = GetLivingWorldNPCs().Find(
        //     delegate(NPCEntity npc) {
        //         if (!_whiteList.Contains(npc)) {
        //             return (npc.HasData(_data));
        //         }
        //         return false;
        //     }
        // );
        if(candidate != null)
            return candidate;

        Debug.LogError("No NPC Fits " + _data.name + ", Returning Random");
        return RandomLivingWorldNPC(_whiteList);

    }

    public static NPCEntity FindNPCByName(string Firstname) {
        NPCEntity candidate = GetLivingWorldNPCs().Find(
            delegate(NPCEntity npc) {
                return (npc.data.fullName.ToLower().Contains(Firstname.ToLower()));
            }
        );
        if(candidate == null)
            Debug.LogError("candidate: " + Firstname + " is not in World");
        return candidate;
    }

    public static Dictionary<string, NPCEntity> NPCDataFitsProduction(NPCEntity _entity, List<NPCEntity> _whiteList, List<AbstractRelation> _abstractRelationConditions) {

        // LivingEntityData entityData = _entity.livingData;
        Dictionary<string, NPCEntity> output = new Dictionary<string, NPCEntity>();
        List<NPCEntity> whiteList = new List<NPCEntity>();
        List<NPCRelation> usedNPCRelation = new List<NPCRelation>();

        output.Add("#", _entity);
        foreach(AbstractRelation item in _abstractRelationConditions) {
            // whiteList.AddRange(output.Values);
            // whiteList.Remove(output["#"]);

            NPCEntity e = _entity;

            if(!output.ContainsKey(item.firstPartyDataName)) {
                Dictionary<string, NPCEntity> d = FindNPCWithRelationship(whiteList, item);
                foreach(var i in d) {
                    if(output.Contains(i)) continue;
                    output.Add(i.Key, i.Value);
                }
                e = output[item.firstPartyDataName];

            } else {
                e = output[item.firstPartyDataName];
            }

            if(!output.ContainsKey(item.secondPartyDataName)) {

                Dictionary<string, NPCEntity> d = FindNPCApartOfRelationship(e, whiteList, item);
                foreach(var i in d) {
                    if(output.Contains(i)) continue;
                    output.Add(i.Key, i.Value);
                }
            }
        }
        return output;
    }

    public enum FindNPCByRelation {
        FIRSTPARTY,
        SECONDPARTY

    }
    public static Dictionary<string, NPCEntity> FindNPCWithRelationship(List<NPCEntity> _whiteList, AbstractRelation _abstractRelation) {
        Dictionary<string, NPCEntity> output = new Dictionary<string, NPCEntity>();

        NPCEntity e;
        while (!output.Any()) {
            e = RandomLivingWorldNPC();
            if(e.HasRelationship(_abstractRelation))
                output.Add(_abstractRelation.firstPartyDataName, e);
        }
        return output;
    }

    public static Dictionary<string, NPCEntity> FindNPCApartOfRelationship(NPCEntity _baceNpc, List<NPCEntity> _whiteList, AbstractRelation _abstractRelation) {

        Dictionary<string, NPCEntity> output = new Dictionary<string, NPCEntity>();

        string name = (_abstractRelation.secondPartyDataType == DataType.Specify) ?
            _abstractRelation.secondPartySpecifiedData.name : _abstractRelation.secondPartyDataName;

        foreach(var relation in _baceNpc.ActiveRelations) {
            NPCEntity ent;

            switch (_abstractRelation.secondPartyDataType) {
                case DataType.TraitsData:
                    if(relation.trait as TraitsData) {
                        ent = WorldData.FindNPCByData(relation.trait, _whiteList);
                        output.Add(name, ent);
                        return output;
                    }
                    break;
                case DataType.NPCData:
                    if(relation.trait as TraitsData) {
                        ent = WorldData.FindNPCByData(relation.trait, _whiteList);
                        output.Add(name, ent);
                    }
                    break;
                case DataType.Data:
                    ent = WorldData.FindNPCByData(relation.trait, _whiteList);
                    output.Add(name, ent);
                    return output;
                    break;
                case DataType.RaceData:
                    if(relation.trait as RaceData) {
                        ent = WorldData.FindNPCByData(relation.trait, _whiteList);
                        output.Add(name, ent);
                        return output;
                    }
                    break;
            }
        }
        return output;
    }
    // public static Dictionary<string, NPCEntity> NPCDataFitsProduction(LivingEntity _entity, List<NPCEntity> _whiteList, List<AbstractRelation> _abstractRelationConditions) {

    //     // LivingEntityData entityData = _entity.livingData;
    //     Dictionary<string, NPCEntity> output = new Dictionary<string, NPCEntity>();
    //     List<NPCEntity> whiteList = new List<NPCEntity>();
    //     List<NPCRelation> usedNPCRelation = new List<NPCRelation>();

    //     foreach (AbstractRelation item in _abstractRelationConditions) {
    //         whiteList.AddRange(output.Values);
    //         if (!output.ContainsKey(item.secondPartyDataName)) {
    //             NPCEntity outputEntity = null;
    //             switch (item.secondPartyDataType) {
    //                 case DataType.TraitsData:
    //                     NPCRelation traitsDataRelation = _entity.ActiveRelations.Find(
    //                         delegate(NPCRelation r) {
    //                             return (r.CurruntRelation()== item.relation &&
    //                                 r.trait.GetType().ToString()== DataType.TraitsData.ToString()&&
    //                                 !usedNPCRelation.Contains(r));
    //                         });

    //                     TraitsData traitsData = null;
    //                     if (traitsDataRelation != null) {
    //                         usedNPCRelation.Add(traitsDataRelation);

    //                         traitsData = (TraitsData)traitsDataRelation.trait;
    //                         NPCEntity nPC = WorldData.FindNPCByData(traitsData, whiteList);
    //                         if (nPC == null)
    //                             Debug.LogError(traitsData.name + " is not in world ");
    //                         outputEntity = nPC;
    //                     } else {
    //                         Debug.LogError("nothing for " + _entity.data);
    //                     }
    //                     break;

    //                 case DataType.NPCData:
    //                     Debug.Log(item.secondPartyDataName);
    //                     NPCRelation npcDataRelation = _entity.ActiveRelations.Find(
    //                         delegate(NPCRelation r) {
    //                             return (r.CurruntRelation()== item.relation &&
    //                                 r.trait.GetType().ToString()== DataType.NPCData.ToString()&&
    //                                 !usedNPCRelation.Contains(r));
    //                         });

    //                     LivingEntityData npcdata = null;
    //                     if (npcDataRelation != null) {
    //                         usedNPCRelation.Add(npcDataRelation);

    //                         npcdata = (LivingEntityData)npcDataRelation.trait;
    //                         NPCEntity nPC = WorldData.FindNPCByData(npcdata, whiteList);
    //                         if (nPC == null)
    //                             Debug.LogError(npcdata.name + " is not in world ");

    //                         outputEntity = nPC;
    //                     } else {
    //                         Debug.LogError("nothing for " + _entity.data);
    //                     }
    //                     break;

    //                 case DataType.RaceData:
    //                     NPCRelation raceDataRelation = _entity.ActiveRelations.Find(
    //                         delegate(NPCRelation r) {
    //                             return (r.CurruntRelation()== item.relation &&
    //                                 r.trait.GetType().ToString()== DataType.RaceData.ToString()&&
    //                                 !usedNPCRelation.Contains(r));
    //                         });
    //                     RaceData raceData = null;
    //                     if (raceDataRelation != null) {
    //                         usedNPCRelation.Add(raceDataRelation);

    //                         raceData = (RaceData)raceDataRelation.trait;
    //                         NPCEntity nPC = WorldData.FindNPCByData(raceData, whiteList);
    //                         if (nPC == null)
    //                             Debug.LogError(raceData.name + " is not in world ");
    //                         outputEntity = nPC;
    //                     } else {
    //                         Debug.LogError("nothing for " + _entity.data);
    //                     }
    //                     break;
    //                 case DataType.Data:
    //                     NPCRelation DataRelation = _entity.ActiveRelations.Find(
    //                         delegate(NPCRelation r) {
    //                             return (r.CurruntRelation()== item.relation &&
    //                                 !usedNPCRelation.Contains(r));
    //                         });
    //                     Data data = null;
    //                     if (DataRelation != null) {
    //                         usedNPCRelation.Add(DataRelation);

    //                         data = (Data)DataRelation.trait;
    //                         NPCEntity nPC = WorldData.FindNPCByData(data, whiteList);
    //                         if (nPC == null)
    //                             Debug.LogError(data.name + " is not in world ");
    //                         outputEntity = nPC;
    //                     } else {
    //                         Debug.LogError("nothing for " + _entity.data);
    //                     }
    //                     break;
    //                 case DataType.Specify:
    //                     NPCRelation SpecifiedRelation = _entity.ActiveRelations.Find(
    //                         delegate(NPCRelation r) {
    //                             return (r.CurruntRelation()== item.relation &&
    //                                 r.trait.HasData(item.secondPartySpecifiedData)&&
    //                                 !usedNPCRelation.Contains(r));
    //                         });
    //                     Data Specifieddata = null;
    //                     if (SpecifiedRelation != null) {
    //                         usedNPCRelation.Add(SpecifiedRelation);

    //                         Specifieddata = (Data)SpecifiedRelation.trait;
    //                         NPCEntity nPC = WorldData.FindNPCByData(Specifieddata, whiteList);
    //                         if (nPC == null)
    //                             Debug.LogError(Specifieddata.name + " is not in world ");
    //                         outputEntity = nPC;
    //                     } else {
    //                         Debug.LogError("nothing for " + _entity.data);
    //                     }
    //                     break;
    //             }
    //             if (outputEntity != null) {
    //                 if (item.secondPartyDataType == DataType.Specify)
    //                     output.Add(item.secondPartySpecifiedData.name, outputEntity);
    //                 else
    //                     output.Add(item.secondPartyDataName, outputEntity);
    //             } else {
    //                 if (item.secondPartyDataType == DataType.Specify)
    //                     output.Add(item.secondPartySpecifiedData.name, WorldData.RandomLivingWorldNPCWithData(item.secondPartySpecifiedData, whiteList));
    //                 else
    //                     output.Add(item.secondPartyDataName, WorldData.RandomLivingWorldNPC(whiteList));
    //             }
    //         }
    //     }
    //     return output;
    // }

    private void Awake() {
        main = this;
    }
}