﻿
Shader "PlayerShipShader" {
 
    Properties {
 
        _Color ("Main Color", Color) = (1,1,1,0.5)
 
        _MainTex ("Base (RGB)", 2D) = "white" {}

         [Toggle(Mask_Head)]
        _MaskHead ("Mask Head", Float) = 0

		[Toggle(Mask_Scalp)]
        _MaskScalp ("Mask Scalp", Float) = 0

		[Toggle(Mask_Body)]
        _MaskBody ("Mask Body", Float) = 0

		[Toggle(Mask_Arms)]
        _MaskArms ("Mask Arms", Float) = 0

		[Toggle(Mask_Legs)]
        _MaskLegs ("Mask Legs", Float) = 0

		[Toggle(Mask_Feet)]
        _MaskFeet ("Mask Feet", Float) = 0
    }
 
    SubShader {
 
        Tags {"Queue" = "Geometry"}
		Tags { "RenderType" = "Opaque" }
      	Cull Off 
       
 
        CGPROGRAM
// Upgrade NOTE: excluded shader from OpenGL ES 2.0 because it uses non-square matrices
#pragma exclude_renderers gles
 
      	#pragma surface surf Lambert
		
		#pragma shader_feature Mask_Head
		#pragma shader_feature Mask_Scalp
		#pragma shader_feature Mask_Body
		#pragma shader_feature Mask_Body
		#pragma shader_feature Mask_Arms
		#pragma shader_feature Mask_Legs
		#pragma shader_feature Mask_Feet

 
 
        float4 _Color;
 
        sampler2D _MainTex;

 
        struct Input {
 
            float4 screenPos;
 
            float4 vertColor : COLOR;
 
        };
 
		float3x4 _MaskColors;

 
        void surf (Input IN, inout SurfaceOutput o) {
 
            float2 screenUV = IN.screenPos.xy / IN.screenPos.w;
 
            half4 c = tex2D (_MainTex, screenUV);
 
            c = c * _Color * IN.vertColor;
 
            o.Albedo = c.rgb;
			o.Alpha = c.a;

			#ifdef Mask_Head
			if(IN.vertColor.r > 0.5 && IN.vertColor.g < 0.5 && IN.vertColor.b < 0.5)
				o.Alpha = 0;
			#endif
			#ifdef Mask_Arms
			if(IN.vertColor.r < 0.5 && IN.vertColor.g > 0.5 && IN.vertColor.b < 0.5)
				o.Alpha = 0;
			#endif
			#ifdef Mask_Body
			if(IN.vertColor.r < 0.5 && IN.vertColor.g < 0.5 && IN.vertColor.b > 0.5)
				o.Alpha = 0;
			#endif
			#ifdef Mask_Leg
			if(IN.vertColor.r < 0.5 && IN.vertColor.g > 0.5 && IN.vertColor.b > 0.5)
				o.Alpha = 0;
			#endif
			#ifdef Mask_Head
			if(IN.vertColor.r > 0.5 && IN.vertColor.g < 0.5 && IN.vertColor.b < 0.5)
				o.Alpha = 0;
			#endif

        }
 
        ENDCG
 
    }
 
}