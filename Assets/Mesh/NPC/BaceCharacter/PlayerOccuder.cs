using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SkinnedMeshRenderer))]
public class PlayerOccuder : MonoBehaviour {

    public Color32 mask;

    [ContextMenu("Invert")]
    void Invert() {
        SkinnedMeshRenderer filter = GetComponent(typeof(SkinnedMeshRenderer)) as SkinnedMeshRenderer;
        if(filter != null) {
            Mesh mesh = filter.sharedMesh;

            Vector3[] normals = mesh.normals;
            for(int i = 0; i < normals.Length; i++)
                normals[i] = -normals[i];

            mesh.normals = normals;

            for(int m = 0; m < mesh.subMeshCount; m++) {
                int[] triangles = mesh.GetTriangles(m);
                Vector3[] vertices = mesh.vertices;
                List<Color32> colors = new List<Color32>();
                mesh.GetColors(colors);

                for(int i = 0; i < triangles.Length; i += 3) {
                    print(colors[triangles[i]]);

                    if(Equals(colors[triangles[i]], mask)) {
                        int temp = triangles[i + 0];
                        triangles[i + 0] = triangles[i + 1];
                        triangles[i + 1] = temp;
                    }
                }
                mesh.SetTriangles(triangles, m);
            }
        }
    }

}